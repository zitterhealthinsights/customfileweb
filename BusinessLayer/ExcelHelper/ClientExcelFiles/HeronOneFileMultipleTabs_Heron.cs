﻿using BusinessLayer.CommonUtill;
using BusinessLayer.ExcelHelper.Interface;
using BusinessLayer.Models;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table.PivotTable;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml;

namespace BusinessLayer.ExcelHelper.ClientExcelFiles
{
    public class HeronOneFileMultipleTabs_Heron : IExcelFileInfo
    {
        private int processId = 21;

        public int ProcessId
        {
            get
            {
                return this.processId;
            }
        }

        public string ProcessTypeName
        {
            get
            {
                return GetProcessNameByProcessId();
            }
        }

        static private DataTable ResultsData = new DataTable();

        List<HeronOneFileMultipleTabs_Heron_PivotTableViewModel> pivotList = null;

        public HeronOneFileMultipleTabs_Heron()
        {
            pivotList = new List<HeronOneFileMultipleTabs_Heron_PivotTableViewModel>();
            var sheet1 = new HeronOneFileMultipleTabs_Heron_PivotTableViewModel()
            {
                SheetName = "CINV by State",
                PivotSheetName = "CINV Recent Changes by State",
                PivotColumnList = new List<FieldTypeAndName>() {
                                    new FieldTypeAndName() {FieldType= "RowFields",ColumnName= "State"},
                                    new FieldTypeAndName() {FieldType= "RowFields",ColumnName= "PlanName"},
                                    new FieldTypeAndName() {FieldType= "RowFields",ColumnName= "Reason_for_Change_Details"},
                                    new FieldTypeAndName() {FieldType= "PageFields",ColumnName= "Segment"},
                                    new FieldTypeAndName() {FieldType= "PageFields",ColumnName= "Drug_Name"},
                                    new FieldTypeAndName() {FieldType= "PageFields",ColumnName= "Change_to_Entry"},
                                    new FieldTypeAndName() {FieldType= "DataFields",ColumnName= "Medical Lives"},
                }
            };

            var sheet2 = new HeronOneFileMultipleTabs_Heron_PivotTableViewModel()
            {
                SheetName = "CINV",
                PivotSheetName = "CINV Recent Changes",
                PivotColumnList = new List<FieldTypeAndName>() {
                                    new FieldTypeAndName() {FieldType= "RowFields",ColumnName= "Plan_Name"},
                                    new FieldTypeAndName() {FieldType= "RowFields",ColumnName= "Reason_for_Change_Details"},
                                    new FieldTypeAndName() {FieldType= "PageFields",ColumnName= "Segment"},
                                    new FieldTypeAndName() {FieldType= "PageFields",ColumnName= "Drug_Name"},
                                    new FieldTypeAndName() {FieldType= "PageFields",ColumnName= "Change_to_Entry"},
                                    new FieldTypeAndName() {FieldType= "DataFields",ColumnName= "PlanMedicalLives"},
                }
            };

            pivotList.Add(sheet1);
            pivotList.Add(sheet2);
        }

        public ExcelExportInfo DownloadZipFile(int processId, string clientName, string indication, SqlDataReader reader, string exportFilePath, string folderPath)
        {
            string fileSaveInFolderPath = $"{exportFilePath}/{ clientName + " - " + ProcessTypeName.Replace('/', ' ')}";
            List<Dictionary<string, object>> dic = null;
            using (var excelPackage = new ExcelPackage())
            {
                ExcelWorksheet userSheet = null;
                bool isAddedExcelSheets = false;
                int sheetIndex = 0;
                while (reader.HasRows)
                {
                    dic = new List<Dictionary<string, object>>();
                    DataTable dtSchema = reader.GetSchemaTable();

                    int rowIndex = 2;
                    if (!isAddedExcelSheets)
                    {
                        // Call Read before accessing data. 
                        while (reader.Read())
                        {
                            if (!isAddedExcelSheets)
                            {
                                Dictionary<string, object> dict = new Dictionary<string, object>();
                                dict = Enumerable.Range(0, reader.FieldCount).ToDictionary(reader.GetName, reader.GetValue);
                                dic.Add(dict);
                            }
                        }
                    }
                    else
                    {
                        List<string> rows = null;
                        var worksheet = excelPackage.Workbook.Worksheets[sheetIndex];
                        worksheet.Cells["A1"].LoadFromDataReader(reader, true);

                        switch (worksheet.Name.Replace(" ", "_").ToLower())
                        {
                            case "cinv_by_state":
                                for (int i = 2; i <= worksheet.Dimension.End.Row; i++)
                                {
                                    worksheet.Cells[i, 12].Value = Convert.ToInt32(worksheet.Cells[i, 12].Value);
                                }
                                AddExcelHeader(worksheet);
                                break;
                            case "cinv":
                                for (int i = 2; i <= worksheet.Dimension.End.Row; i++)
                                {
                                    worksheet.Cells[i, 16].Value = Convert.ToInt32(worksheet.Cells[i, 16].Value);
                                }
                                AddExcelHeader(worksheet);
                                break;
                            case "cinv_dd":
                                var dataRange = worksheet.Cells[worksheet.Dimension.Address];
                                worksheet.Cells[1, 1].Style.Font.Bold = true;
                                worksheet.Cells[1, 2].Style.Font.Bold = true;
                                worksheet.Cells[1, 3].Style.Font.Bold = true;
                                worksheet.Cells.Style.Font.Name = "Arial";

                                // Assign borders
                                //dataRange.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                                //dataRange.Style.Border.Left.Style = ExcelBorderStyle.Medium;
                                //dataRange.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                                //dataRange.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                                dataRange.AutoFitColumns();

                                for (int row = 1; row <= worksheet.Dimension.End.Row; row++)
                                {

                                    for (int colIndex = 1; colIndex <= worksheet.Dimension.End.Column; colIndex++)
                                    {
                                        if (row == 1)
                                        {
                                            worksheet.Cells[row, colIndex].Style.Font.Size = 12;
                                        }
                                        worksheet.Cells[row, colIndex].Style.WrapText = true;
                                        worksheet.Column(colIndex).Width = 60;
                                    }
                                }

                                for (int col = 1; col <= 2; col++)
                                {
                                    MergeCell(worksheet, col);
                                }

                                //Remove Empty Excel Row
                                worksheet.TrimLastEmptyRows();
                                break;
                            case "definition":
                                for (var row = 1; row <= worksheet.Dimension.End.Row; row++)
                                {
                                    worksheet.Cells[row, 1].Style.Font.Bold = true;
                                    worksheet.Cells[row, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                }
                                worksheet.Cells[1, 2].Style.Font.Bold = true;
                                var dataRangeSheet = worksheet.Cells[worksheet.Dimension.Address];
                                dataRangeSheet.AutoFitColumns();
                                break;
                        }                        
                        SaveExcelFile(excelPackage, fileSaveInFolderPath, "", clientName, indication);
                    }
                    if (!isAddedExcelSheets)
                    {
                        AddExcelWorkSheet(dic, userSheet, excelPackage, ref isAddedExcelSheets);
                    }
                    sheetIndex++;
                    reader.NextResult();
                }

                // Add Pivot Table                
                foreach (var pivotTabel in pivotList)
                {
                    AddPivot_Table(excelPackage, pivotTabel);
                }

                //Save Excel file
                fileSaveInFolderPath = SaveExcelFile(excelPackage, fileSaveInFolderPath, folderPath, clientName, indication);
            }

            //return fileSaveInFolderPath;

            var returnObj = new ExcelExportInfo()
            {
                FilePath = fileSaveInFolderPath,
                ClientName = clientName
            };

            return returnObj;           
        }

        void AddExcelHeader(List<Dictionary<string, object>> excelSheets, ExcelWorksheet sheet)
        {
            var excelHeader = new List<ExcelReportFileProperties>();
            var colIndex = 1;
            foreach (KeyValuePair<string, object> headerData in excelSheets[0])
            {
                excelHeader.Add(new ExcelReportFileProperties { HeaderName = headerData.Key, ColumnIndex = colIndex, RowIndex = 1, Width = 40, Height = 30 });
                colIndex++;
            }
            AddHeader(sheet, excelHeader);
        }

        void AddHeader(ExcelWorksheet sheet, IList<ExcelReportFileProperties> headers)
        {
            foreach (var header in headers)
            {

                sheet.Cells[header.RowIndex, header.ColumnIndex].Value = header.HeaderName;
                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.Font.Bold = true;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.WrapText = true;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                sheet.Column(header.ColumnIndex).Width = header.Width;

                sheet.Row(header.RowIndex).Height = header.Height;

            }

        }

        void AddExcelWorkSheet(List<Dictionary<string, object>> excelSheets, ExcelWorksheet userSheet, ExcelPackage excelPackage, ref bool isAddedExcelSheets)
        {
            foreach (Dictionary<string, object> sheet in excelSheets)
            {
                foreach (KeyValuePair<string, object> sheetName in sheet)
                {
                    userSheet = excelPackage.Workbook.Worksheets.Add(sheetName.Value.ToString());
                    userSheet.OutLineApplyStyle = true;
                }
            }

            isAddedExcelSheets = true;
        }

        string SaveExcelFile(ExcelPackage excelPackage, string fileSaveInFolderPath, string folderPath, string clientName, string indication)
        {
            try
            {
                // var tempFilePath = Path.GetTempPath(); 
                var tempFilePath = fileSaveInFolderPath;
                if (!string.IsNullOrEmpty(folderPath))
                {
                    //tempFilePath = tempFilePath.Split('\\').ToString();
                    tempFilePath = folderPath;
                }
                else
                {
                    if (!Directory.Exists(tempFilePath))
                        Directory.CreateDirectory(tempFilePath);
                }
                //var filePath = Path.Combine(tempFilePath, ProcessTypeName + (string.IsNullOrEmpty(folderPath) == true ? "" : "-2") + ".xlsx");
                var filePath = Path.Combine(tempFilePath, clientName + "-" + indication + ".xlsx");

                excelPackage.SaveAs(new FileInfo(filePath));
                return tempFilePath;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }


        }

        void AddDataToSheet(ExcelWorksheet sheet, List<Dictionary<string, object>> dataList)
        {
            for (int i = 0; i < dataList.Count(); i++)
            {
                var rowIndex = i + 2;
                var data = dataList[i];
                int colIndex = 1;
                foreach (var valueData in data)
                {
                    sheet.Cells[rowIndex, colIndex].Value = valueData.Value;
                    colIndex++;
                }

            }
        }

        string GetProcessNameByProcessId()
        {
            string processTypeName = string.Empty;

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["CustomFileGeneratorDbContext"].ConnectionString))
            {
                connection.Open();
                string strQuery = "select * from process where processId = @processId";
                SqlCommand cmd = new SqlCommand(strQuery, connection);
                cmd.Parameters.AddWithValue("@processId", processId);

                // get query results

                SqlDataReader rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    processTypeName = rdr["ProcessName"].ToString();
                }

                return processTypeName;
            }
        }

        void AddExcelHeader(ExcelWorksheet worksheet)
        {
            // set autofilter
            worksheet.Cells[worksheet.Dimension.Address].AutoFilter = true;
            for (int col = 1; col <= worksheet.Dimension.End.Column; col++)
            {
                worksheet.Column(col).Width = 16;
                worksheet.Cells[1, col].Style.Font.Bold = true;
                worksheet.Cells[1, col].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[1, col].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(191, 191, 191));
            }
        }

        void AddPivot_Table(ExcelPackage excelPackage, HeronOneFileMultipleTabs_Heron_PivotTableViewModel objPivot)
        {
            var userSheet = excelPackage.Workbook.Worksheets[objPivot.PivotSheetName];

            //define the data range on the source sheet
            var worksheetData = excelPackage.Workbook.Worksheets[objPivot.SheetName];
            var dataRange = worksheetData.Cells[worksheetData.Dimension.Address];

            //create the pivot table
            var pivotTable = userSheet.PivotTables.Add(userSheet.Cells["A5"], dataRange, "PivotTable");

            //label field
            foreach (var data in objPivot.PivotColumnList.Where(x => x.FieldType == "RowFields"))
            {
                pivotTable.RowFields.Add(pivotTable.Fields[data.ColumnName]);
                pivotTable.DataOnRows = true;
            }

            //Report Filter
            foreach (var data in objPivot.PivotColumnList.Where(x => x.FieldType == "PageFields"))
            {
                pivotTable.PageFields.Add(pivotTable.Fields[data.ColumnName]);
            }

            //data fields
            foreach (var data in objPivot.PivotColumnList.Where(x => x.FieldType == "DataFields"))
            {
                var field = pivotTable.DataFields.Add(pivotTable.Fields[data.ColumnName]);
                field.Function = DataFieldFunctions.Sum;
            }

        }

        void MergeCell_BCK(ExcelWorksheet worksheet, int colIndex)
        {
            int lastRowIndex = 0;
            for (int row = 1; row <= worksheet.Dimension.End.Row; row++)
            {
                if (string.IsNullOrEmpty(worksheet.Cells[row, colIndex].Value.ToString()))
                {
                    if (lastRowIndex == 0)
                        lastRowIndex = row;
                }
                else if (lastRowIndex != 0 && (!string.IsNullOrEmpty(worksheet.Cells[row, 1].Value.ToString())))
                {
                    worksheet.Cells[lastRowIndex - 1, colIndex, row - 1, colIndex].Merge = true;
                    lastRowIndex = 0;
                }
            }
        }

        void MergeCell(ExcelWorksheet worksheet, int colIndex)
        {
            int lastRowIndex = 0;
            for (int row = 1; row <= worksheet.Dimension.End.Row; row++)
            {
                worksheet.Cells[row, 1].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                worksheet.Cells[row, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                worksheet.Cells[row, 1].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                worksheet.Cells[row, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;

                worksheet.Cells[row, 2].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                worksheet.Cells[row, 2].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                worksheet.Cells[row, 2].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                worksheet.Cells[row, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;

                worksheet.Cells[row, 3].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                worksheet.Cells[row, 3].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                worksheet.Cells[row, 3].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                worksheet.Cells[row, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;

                if (row != 1)
                {
                    worksheet.Cells[row, 1].Style.Font.Size = 10;
                    worksheet.Cells[row, 2].Style.Font.Size = 9;
                    worksheet.Cells[row, 3].Style.Font.Size = 8;
                }
                if (string.IsNullOrEmpty(worksheet.Cells[row, colIndex].Value.ToString()))
                {
                    if (lastRowIndex == 0)
                        lastRowIndex = row;

                    //check rows have first column is null for remove border
                    //if first column is empty then apply border on last column
                    if (string.IsNullOrEmpty(worksheet.Cells[row, 1].Value.ToString()))
                    {
                        worksheet.Cells[row, 3].Style.Border.Top.Style = ExcelBorderStyle.None;
                        worksheet.Cells[row, 3].Style.Border.Left.Style = ExcelBorderStyle.None;
                        worksheet.Cells[row, 3].Style.Border.Right.Style = ExcelBorderStyle.None;
                        worksheet.Cells[row, 3].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                    }

                }
                else if (lastRowIndex != 0 && (!string.IsNullOrEmpty(worksheet.Cells[row, 1].Value.ToString())))
                {
                    worksheet.Cells[lastRowIndex - 1, colIndex, row - 1, colIndex].Merge = true;
                    worksheet.Cells[lastRowIndex - 1, colIndex, row - 1, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    for (int rowValue = lastRowIndex - 1; rowValue <= row - 1; rowValue++)
                    {
                        worksheet.Cells[rowValue, 3].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                        worksheet.Cells[rowValue, 3].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                        worksheet.Cells[rowValue, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Dotted;
                        worksheet.Cells[rowValue, 3].Style.Font.Size = 8;

                        //apply border on merged cell in last column
                        if (rowValue == row - 1)
                        {
                            worksheet.Cells[rowValue, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                        }
                    }

                    lastRowIndex = 0;
                }
            }
        }
    }
}
