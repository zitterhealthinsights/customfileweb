﻿using BusinessLayer.ExcelHelper.Interface;
using BusinessLayer.Models;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.ExcelHelper.ClientExcelFiles
{
    public class McCannHCHLStateFile_RegeneronSanofi : IExcelFileInfo
    {
        private int processId = 19;

        public int ProcessId
        {
            get
            {
                return this.processId;
            }
        }

        public string ProcessTypeName
        {
            get
            {
                return GetProcessNameByProcessId();
            }
        }

        public ExcelExportInfo DownloadZipFile(int processId, string clientName, string indication, SqlDataReader reader, string exportFilePath, string folderPath)
        {
            string fileSaveInFolderPath = $"{exportFilePath}/{ clientName +" - "+ ProcessTypeName}";
            List<Dictionary<string, object>> dic = null;
            using (var excelPackage = new ExcelPackage())
            {
                ExcelWorksheet userSheet = null;
                bool isAddedExcelSheets = false;
                int sheetIndex = 0;
                while (reader.HasRows || reader.FieldCount > 0)
                {
                    dic = new List<Dictionary<string, object>>();
                    DataTable dtSchema = reader.GetSchemaTable();

                    int rowIndex = 2;
                    if (!isAddedExcelSheets)
                    {
                        // Call Read before accessing data. 
                        while (reader.Read())
                        {
                            if (!isAddedExcelSheets)
                            {
                                Dictionary<string, object> dict = new Dictionary<string, object>();
                                dict = Enumerable.Range(0, reader.FieldCount).ToDictionary(reader.GetName, reader.GetValue);
                                dic.Add(dict);
                            }
                        }
                    }
                    else
                    {
                        List<string> rows = null;
                        var worksheet = excelPackage.Workbook.Worksheets[sheetIndex];
                        worksheet.Cells["A1"].LoadFromDataReader(reader, true);
                        switch (worksheet.Name.ToLower())
                        {
                            case "definition":
                                for (var row = 1; row <= worksheet.Dimension.End.Row; row++)
                                {
                                    worksheet.Cells[row, 1].Style.Font.Bold = true;
                                    worksheet.Cells[row, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                }
                                worksheet.Cells[1, 2].Style.Font.Bold = true;
                                var dataRangeSheet = worksheet.Cells[worksheet.Dimension.Address];
                                dataRangeSheet.AutoFitColumns();
                                break;
                            default:
                                AddExcelHeader(worksheet);
                                break;
                        }
                        SaveExcelFile(excelPackage, fileSaveInFolderPath, "", clientName, indication);
                    }
                    if (!isAddedExcelSheets)
                    {
                        AddExcelWorkSheet(dic, userSheet, excelPackage, ref isAddedExcelSheets);
                    }
                    sheetIndex++;
                    reader.NextResult();
                }

                fileSaveInFolderPath = SaveExcelFile(excelPackage, fileSaveInFolderPath, folderPath, clientName, indication);
            }
            //return fileSaveInFolderPath;
            var returnObj = new ExcelExportInfo()
            {
                FilePath = fileSaveInFolderPath,
                ClientName = clientName
            };
            return returnObj;
        }

        void AddExcelHeader(List<Dictionary<string, object>> excelSheets, ExcelWorksheet sheet)
        {
            var excelHeader = new List<ExcelReportFileProperties>();
            var colIndex = 1;
            foreach (KeyValuePair<string, object> headerData in excelSheets[0])
            {
                excelHeader.Add(new ExcelReportFileProperties { HeaderName = headerData.Key, ColumnIndex = colIndex, RowIndex = 1, Width = 40, Height = 30 });
                colIndex++;
            }
            AddHeader(sheet, excelHeader);
        }

        void AddHeader(ExcelWorksheet sheet, IList<ExcelReportFileProperties> headers)
        {
            foreach (var header in headers)
            {

                sheet.Cells[header.RowIndex, header.ColumnIndex].Value = header.HeaderName;
                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.Font.Bold = true;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.WrapText = true;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                sheet.Column(header.ColumnIndex).Width = header.Width;

                sheet.Row(header.RowIndex).Height = header.Height;

            }

        }

        string SaveExcelFile(ExcelPackage excelPackage, string fileSaveInFolderPath, string folderPath)
        {
            try
            {
                // var tempFilePath = Path.GetTempPath(); 
                var tempFilePath = fileSaveInFolderPath;
                if (!string.IsNullOrEmpty(folderPath))
                {
                    tempFilePath = folderPath;
                }
                else
                {
                    if (!Directory.Exists(tempFilePath))
                        Directory.CreateDirectory(tempFilePath);

                }
                var filePath = Path.Combine(tempFilePath, ProcessTypeName + (string.IsNullOrEmpty(folderPath) == true ? "" : "-2") + ".xlsx");

                excelPackage.SaveAs(new FileInfo(filePath));
                return tempFilePath;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }


        }

        void AddDataToSheet(ExcelWorksheet sheet, List<Dictionary<string, object>> dataList)
        {
            for (int i = 0; i < dataList.Count(); i++)
            {
                var rowIndex = i + 2;
                var data = dataList[i];
                int colIndex = 1;
                foreach (var valueData in data)
                {
                    sheet.Cells[rowIndex, colIndex].Value = valueData.Value;
                    colIndex++;
                }

            }
        }

        string SaveExcelFile(ExcelPackage excelPackage, string fileSaveInFolderPath, string folderPath, string clientName, string indication)
        {
            try
            {
                // var tempFilePath = Path.GetTempPath(); 
                var tempFilePath = fileSaveInFolderPath;
                if (!string.IsNullOrEmpty(folderPath))
                {
                    //tempFilePath = tempFilePath.Split('\\').ToString();
                    tempFilePath = folderPath;
                }
                else
                {
                    if (!Directory.Exists(tempFilePath))
                        Directory.CreateDirectory(tempFilePath);
                }
                //var filePath = Path.Combine(tempFilePath, ProcessTypeName + (string.IsNullOrEmpty(folderPath) == true ? "" : "-2") + ".xlsx");
                var filePath = Path.Combine(tempFilePath, clientName + "-" + indication + ".xlsx");

                excelPackage.SaveAs(new FileInfo(filePath));
                return tempFilePath;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        void AddExcelWorkSheet(List<Dictionary<string, object>> excelSheets, ExcelWorksheet userSheet, ExcelPackage excelPackage, ref bool isAddedExcelSheets)
        {
            foreach (Dictionary<string, object> sheet in excelSheets)
            {
                foreach (KeyValuePair<string, object> sheetName in sheet)
                {
                    userSheet = excelPackage.Workbook.Worksheets.Add(sheetName.Value.ToString());
                    userSheet.OutLineApplyStyle = true;
                }
            }

            isAddedExcelSheets = true;
        }

        void AddExcelHeader(ExcelWorksheet worksheet)
        {
            // set autofilter
            worksheet.Cells[worksheet.Dimension.Address].AutoFilter = true;
            for (int col = 1; col <= worksheet.Dimension.End.Column; col++)
            {
                worksheet.Column(col).Width = 16;
                worksheet.Cells[1, col].Style.Font.Bold = true;
                worksheet.Cells[1, col].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[1, col].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(191, 191, 191));
            }
        }

        string GetProcessNameByProcessId()
        {
            string processTypeName = string.Empty;

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["CustomFileGeneratorDbContext"].ConnectionString))
            {
                connection.Open();
                string strQuery = "select * from process where processId = @processId";
                SqlCommand cmd = new SqlCommand(strQuery, connection);
                cmd.Parameters.AddWithValue("@processId", processId);

                // get query results

                SqlDataReader rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    processTypeName = rdr["ProcessName"].ToString();
                }

                return processTypeName;
            }
        }
    }
}
