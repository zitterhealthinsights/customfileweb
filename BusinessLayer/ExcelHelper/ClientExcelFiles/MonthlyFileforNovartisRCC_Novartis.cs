﻿using BusinessLayer.ExcelHelper.Interface;
using BusinessLayer.Models;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table.PivotTable;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BusinessLayer.ExcelHelper.ClientExcelFiles
{
    public class MonthlyFileforNovartisRCC_Novartis : IExcelFileInfo
    {
        private int processId = 31;

        public int ProcessId
        {
            get
            {
                return this.processId;
            }
        }

        public string ProcessTypeName
        {
            get
            {
                return GetProcessNameByProcessId();
            }
        }

        List<HeronOneFileMultipleTabs_Heron_PivotTableViewModel> pivotList = null;

        public MonthlyFileforNovartisRCC_Novartis()
        {
            pivotList = new List<HeronOneFileMultipleTabs_Heron_PivotTableViewModel>();

            var sheetRCC = new HeronOneFileMultipleTabs_Heron_PivotTableViewModel()
            {
                SheetName = "RCC Combined",
                PivotSheetName = "Pivots",
                PivotColumnList = new List<FieldTypeAndName>() {
                                    new FieldTypeAndName() {FieldType= "RowFields",ColumnName= "Drug_Name"},
                                    new FieldTypeAndName() {FieldType= "ColumnFields",ColumnName= "Novartis_Healthplan_Management"},
                                    new FieldTypeAndName() {FieldType= "PageFields",ColumnName= "Segment"},
                                    new FieldTypeAndName() {FieldType= "DataFields",ColumnName= "PlanPharmacyLives"}
                }
            };

            pivotList.Add(sheetRCC);
        }

        public ExcelExportInfo DownloadZipFile(int processId, string clientName, string indication, SqlDataReader reader, string exportFilePath, string folderPath)
        {
            string fileSaveInFolderPath = $"{exportFilePath}/{ clientName + " - " + ProcessTypeName }";
            List<Dictionary<string, object>> dic = null;
            using (var excelPackage = new ExcelPackage())
            {
                ExcelWorksheet userSheet = null;
                bool isAddedExcelSheets = false;
                int sheetIndex = 0;
                while (reader.HasRows || reader.FieldCount > 0)
                {
                    dic = new List<Dictionary<string, object>>();
                    DataTable dtSchema = reader.GetSchemaTable();

                    int rowIndex = 2;
                    if (!isAddedExcelSheets)
                    {
                        // Call Read before accessing data. 
                        while (reader.Read())
                        {
                            if (!isAddedExcelSheets)
                            {
                                Dictionary<string, object> dict = new Dictionary<string, object>();
                                dict = Enumerable.Range(0, reader.FieldCount).ToDictionary(reader.GetName, reader.GetValue);
                                dic.Add(dict);
                            }
                        }
                    }
                    else
                    {
                        List<string> rows = null;
                        var worksheet = excelPackage.Workbook.Worksheets[sheetIndex];
                        worksheet.Cells["A1"].LoadFromDataReader(reader, true);

                        switch (worksheet.Name.ToLower())
                        {
                            case "rcc combined":

                                AddExcelHeader(worksheet);
                                
                                //PlanPharmacyLives
                                for (int i = 2; i <= worksheet.Dimension.End.Row; i++)
                                {
                                    worksheet.Cells[i, 17].Value = Convert.ToInt32(worksheet.Cells[i, 17].Value == null ? 0 : worksheet.Cells[i, 17].Value);
                                }

                                //PlanMedicalLives
                                for (int i = 2; i <= worksheet.Dimension.End.Row; i++)
                                {
                                    worksheet.Cells[i, 16].Value = Convert.ToInt32(string.IsNullOrEmpty(worksheet.Cells[i, 16].Value.ToString()) == true ? 0 : worksheet.Cells[i, 16].Value);
                                }
                                break;
                            case "instructions":

                                worksheet.Cells.AutoFitColumns();
                                int refrenceIndex = 0;
                                for (int row=1;row<=worksheet.Dimension.End.Row;row++)
                                {     
                                    if (string.IsNullOrEmpty(worksheet.Cells[row, 1].Value.ToString()))
                                    {
                                        if(refrenceIndex<=1)
                                        {
                                            worksheet.Cells[row, 1].Style.Font.Bold = true;
                                            worksheet.Cells[row, 2].Style.Font.Bold = true;                                            
                                        }
                                        refrenceIndex++;
                                    }

                                    if (refrenceIndex < 1)
                                    {
                                        worksheet.Cells[row, 1].Style.Font.Bold = true;
                                        worksheet.Cells[row, 2].Style.WrapText = true;
                                    }
                                }

                                break;
                        }

                        SaveExcelFile(excelPackage, fileSaveInFolderPath, "", clientName, indication);
                    }
                    if (!isAddedExcelSheets)
                    {
                        AddExcelWorkSheet(dic, userSheet, excelPackage, ref isAddedExcelSheets);
                    }
                    sheetIndex++;
                    reader.NextResult();
                }

                for (int i = 0; i < 2; i++)
                {
                    // Add Pivot Table                
                    foreach (var pivotTabel in pivotList)
                    {
                        AddPivot_Table(excelPackage, pivotTabel, i);
                    }
                }

                fileSaveInFolderPath = SaveExcelFile(excelPackage, fileSaveInFolderPath, folderPath, clientName, indication);
            }
            //return fileSaveInFolderPath;
            var returnObj = new ExcelExportInfo()
            {
                FilePath = fileSaveInFolderPath,
                ClientName = clientName
            };
            return returnObj;
        }

        string SaveExcelFile(ExcelPackage excelPackage, string fileSaveInFolderPath, string folderPath, string clientName, string indication)
        {
            try
            {
                // var tempFilePath = Path.GetTempPath(); 
                var tempFilePath = fileSaveInFolderPath;
                if (!string.IsNullOrEmpty(folderPath))
                {
                    //tempFilePath = tempFilePath.Split('\\').ToString();
                    tempFilePath = folderPath;
                }
                else
                {
                    if (!Directory.Exists(tempFilePath))
                        Directory.CreateDirectory(tempFilePath);
                }
                //var filePath = Path.Combine(tempFilePath, ProcessTypeName + (string.IsNullOrEmpty(folderPath) == true ? "" : "-2") + ".xlsx");
                var filePath = Path.Combine(tempFilePath, clientName + "-" + indication + ".xlsx");

                excelPackage.SaveAs(new FileInfo(filePath));
                return tempFilePath;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        void AddExcelWorkSheet(List<Dictionary<string, object>> excelSheets, ExcelWorksheet userSheet, ExcelPackage excelPackage, ref bool isAddedExcelSheets)
        {
            foreach (Dictionary<string, object> sheet in excelSheets)
            {
                foreach (KeyValuePair<string, object> sheetName in sheet)
                {
                    userSheet = excelPackage.Workbook.Worksheets.Add(sheetName.Value.ToString());
                    userSheet.OutLineApplyStyle = true;
                }
            }

            isAddedExcelSheets = true;
        }

        void AddPivot_Table(ExcelPackage excelPackage, HeronOneFileMultipleTabs_Heron_PivotTableViewModel objPivot, int index)
        {
            var userSheet = excelPackage.Workbook.Worksheets[objPivot.PivotSheetName];

            //define the data range on the source sheet
            var worksheetData = excelPackage.Workbook.Worksheets[objPivot.SheetName];
            var dataRange = worksheetData.Cells[worksheetData.Dimension.Address];

            //Clear Pivot sheet Existing Data
            if (index == 0)
                userSheet.Cells[worksheetData.Dimension.Address].Clear();

            ExcelRange Rng = index == 0 ? userSheet.Cells["A1"] : userSheet.Cells["A17"];
            Rng.Value = index == 0 ? "Health Plan Management Pivot - Pharmacy Drugs" : "Health Plan Management Pivot - Medical Drugs";
            Rng.Style.Font.Bold = true;
            Rng.Style.Font.Color.SetColor(Color.Red);
            var filterCellAddress = index == 0 ? "C3" : "C19";
            userSheet.Cells[filterCellAddress].Value = "<--- (FILTER BY BOOK OF BUSINESS HERE)";

            //create the pivot table
            var pivotTableCellAddress = index == 0 ? "A5" : "A21";
            var pivotTable = userSheet.PivotTables.Add(userSheet.Cells[pivotTableCellAddress], dataRange, index == 0 ? "PivotTable" : "PivotTable_1");

            //label field
            foreach (var data in objPivot.PivotColumnList.Where(x => x.FieldType == "RowFields"))
            {
                pivotTable.RowFields.Add(pivotTable.Fields[data.ColumnName]);
                pivotTable.DataOnRows = false;
            }

            //Report Filter
            foreach (var data in objPivot.PivotColumnList.Where(x => x.FieldType == "PageFields"))
            {
                pivotTable.PageFields.Add(pivotTable.Fields[data.ColumnName]);
            }

            //data fields
            foreach (var data in objPivot.PivotColumnList.Where(x => x.FieldType == "DataFields"))
            {
                var columnValue = index == 0 ? data.ColumnName : "PlanMedicalLives";
                var field = pivotTable.DataFields.Add(pivotTable.Fields[columnValue]);

                field.Function = DataFieldFunctions.Sum;
                field.SetDataFieldShowDataAsAttribute(pivotTable, DataFieldShowDataAs.percentOfRow);
                field.Format = "#0.00%";
            }

            //ExcelPivotTableDataField tableDataField = pivotTable.DataFields[0];
            //tableDataField.SetDataFieldShowDataAsAttribute(pivotTable, DataFieldShowDataAs.percentOfRow);

            //Column fields
            foreach (var data in objPivot.PivotColumnList.Where(x => x.FieldType == "ColumnFields"))
            {
                var field = pivotTable.ColumnFields.Add(pivotTable.Fields[data.ColumnName]);
               
                //field.SubTotalFunctions = eSubTotalFunctions.Sum;
            }
            
            //Set tabular form
            (from pf in pivotTable.Fields
             select pf).ToList().ForEach(f =>
             {
                 f.Outline = false;                 
             });            
        }

        void AddExcelHeader(ExcelWorksheet worksheet)
        {
            // set autofilter
            worksheet.Cells[worksheet.Dimension.Address].AutoFilter = true;
            for (int col = 1; col <= worksheet.Dimension.End.Column; col++)
            {
                worksheet.Column(col).Width = 16;
                worksheet.Cells[1, col].Style.Font.Bold = true;
                worksheet.Cells[1, col].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[1, col].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(191, 191, 191));
            }
        }

        string GetProcessNameByProcessId()
        {
            string processTypeName = string.Empty;

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["CustomFileGeneratorDbContext"].ConnectionString))
            {
                connection.Open();
                string strQuery = "select * from process where processId = @processId";
                SqlCommand cmd = new SqlCommand(strQuery, connection);
                cmd.Parameters.AddWithValue("@processId", processId);

                // get query results

                SqlDataReader rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    processTypeName = rdr["ProcessName"].ToString();
                }

                return processTypeName;
            }
        }
    }

    /// <summary>
    /// Possible values for the ShowDataAs attribute. 
    /// Values got from http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-376,%20Fourth%20Edition,%20Part%201%20-%20Fundamentals%20And%20Markup%20Language%20Reference.zip 
    /// Section 18.8.70
    /// </summary>
    public enum DataFieldShowDataAs
    {
        difference,
        index,
        normal,
        percentDiff,
        percent,
        percentOfCol,
        percentOfRow,
        percentOfTotal,
        runTotal
    }

    public static class PivotMethodExt
    {
        /// <summary>
        /// EPPlus doesn't support the Show Data As for pivot data fields. Set the xml for it with this method.
        /// </summary>
        /// <param name="dataField"></param>
        /// <param name="pivot"></param>
        /// <param name="showDataAs"></param>
        public static void SetDataFieldShowDataAsAttribute(this ExcelPivotTableDataField dataField, ExcelPivotTable pivot, DataFieldShowDataAs showDataAs)
        {
            if (pivot != null & pivot.DataFields != null && pivot.DataFields.Contains(dataField))
            {
                string showDataAsAttributeValue = Enum.GetName(typeof(DataFieldShowDataAs), showDataAs);
                var xml = pivot.PivotTableXml;
                XmlNodeList elements = xml.GetElementsByTagName("dataField");

                foreach (XmlNode elem in elements)
                {
                    XmlAttribute fldAttribute = elem.Attributes["fld"];
                    if (fldAttribute != null && fldAttribute.Value == dataField.Index.ToString())
                    {
                        XmlAttribute showDataAsAttribute = elem.Attributes["showDataAs"];
                        if (showDataAsAttribute == null)
                        {
                            showDataAsAttribute = xml.CreateAttribute("showDataAs");
                            elem.Attributes.InsertAfter(showDataAsAttribute, fldAttribute);
                        }
                        showDataAsAttribute.Value = showDataAsAttributeValue;
                    }
                }

            }
        }

        public static void AddCalculatedField(this OfficeOpenXml.Table.PivotTable.ExcelPivotTable pivotTable, string name, string formula, int numFmtId = 10, int? index = 0, string caption = null)
        {
            if (!pivotTable.WorkSheet.Workbook.Styles.NumberFormats.Where(nf => nf.NumFmtId == numFmtId).Any())
                throw new ArgumentOutOfRangeException("Not a valid numFmtId.");

            //First, add the calculated field cacheFields element as a childr of the cacheFields element in the pivotCacheDefinition1.xml
            XmlElement cacheFieldsElement = pivotTable.CacheDefinition.CacheDefinitionXml.GetElementsByTagName("cacheFields")[0] as XmlElement;

            //Add the cacheField element and take note of the index 
            XmlAttribute cacheFieldsCountAttribute = cacheFieldsElement.Attributes["count"];
            int count = Convert.ToInt32(cacheFieldsCountAttribute.Value);
            cacheFieldsElement.InnerXml += String.Format("<cacheField name=\"{0}\" numFmtId=\"0\" formula=\"{1}\" databaseField=\"0\"/>\n", name, formula);
            int cacheFieldIndex = ++count;
            //update cachefields count attribute
            cacheFieldsCountAttribute.Value = count.ToString();

            //Next, update pivotTable1.xml and insert pivotField element as a child of the pivotFields element
            XmlElement pivotFieldsElement = pivotTable.PivotTableXml.GetElementsByTagName("pivotFields")[0] as XmlElement;
            XmlAttribute pivotFieldsCountAttribute = pivotFieldsElement.Attributes["count"];
            pivotFieldsElement.InnerXml += "<pivotField dataField=\"1\" compact=\"0\" outline=\"0\" subtotalTop=\"0\" dragToRow=\"0\" dragToCol=\"0\" dragToPage=\"0\" showAll=\"0\" includeNewItemsInFilter=\"1\" defaultSubtotal=\"0\"/> \n";
            //update pivotFields count attribute
            pivotFieldsCountAttribute.Value = (int.Parse(pivotFieldsCountAttribute.Value) + 1).ToString();

            //Also in pivotTable1.xml, insert the <dataField> to the correct position, the fld here points to cacheField index
            XmlElement dataFields = pivotTable.PivotTableXml.GetElementsByTagName("dataFields")[0] as XmlElement;

            //Create the dataField element with the attributes
            XmlElement dataField = pivotTable.PivotTableXml.CreateElement("dataField", pivotTable.PivotTableXml.DocumentElement.NamespaceURI);
            dataField.RemoveAllAttributes();
            XmlAttribute nameAttrib = pivotTable.PivotTableXml.CreateAttribute("name");

            //cacheField cannot have same name attribute as dataField
            if (caption == null || caption == name)
                nameAttrib.Value = " " + name;
            else
                nameAttrib.Value = caption;
            dataField.Attributes.Append(nameAttrib);

            XmlAttribute fldAttrib = pivotTable.PivotTableXml.CreateAttribute("fld");

            fldAttrib.Value = (cacheFieldIndex - 1).ToString();
            dataField.Attributes.Append(fldAttrib);
            XmlAttribute baseFieldAttrib = pivotTable.PivotTableXml.CreateAttribute("baseField");
            baseFieldAttrib.Value = "0";
            dataField.Attributes.Append(baseFieldAttrib);
            XmlAttribute baseItemAttrib = pivotTable.PivotTableXml.CreateAttribute("baseItem");
            baseItemAttrib.Value = "0";
            dataField.Attributes.Append(baseItemAttrib);
            XmlAttribute numFmtIdAttrib = pivotTable.PivotTableXml.CreateAttribute("numFmtId");
            numFmtIdAttrib.Value = numFmtId.ToString();
            dataField.Attributes.Append(numFmtIdAttrib);

            //Insert dataField element to the correct position.
            if (index <= 0)
            {
                dataFields.PrependChild(dataField);
            }
            else if (index >= dataFields.ChildNodes.Count)
            {
                dataFields.AppendChild(dataField);
            }
            else
            {
                XmlNode insertBeforeThis = dataFields.ChildNodes.Item(index.Value);
                if (insertBeforeThis != null)
                    dataFields.InsertBefore(dataField, insertBeforeThis);
                else
                    dataFields.AppendChild(dataField);
            }

        }
    }
}
