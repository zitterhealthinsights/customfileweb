﻿using BusinessLayer.ExcelHelper.Interface;
using BusinessLayer.Models;
using OfficeOpenXml;
using OfficeOpenXml.Table.PivotTable;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.ExcelHelper.ClientExcelFiles
{
    public class WeeklyFilesforNovartisEntresto_Novartis : IExcelFileInfo
    {
        private int processId = 23;

        public int ProcessId
        {
            get
            {
                return this.processId;
            }
        }

        public string ProcessTypeName
        {
            get
            {
                return GetProcessNameByProcessId();
            }
        }

        List<HeronOneFileMultipleTabs_Heron_PivotTableViewModel> pivotList = null;

        public WeeklyFilesforNovartisEntresto_Novartis()
        {
            pivotList = new List<HeronOneFileMultipleTabs_Heron_PivotTableViewModel>();
            var sheet1 = new HeronOneFileMultipleTabs_Heron_PivotTableViewModel()
            {
                SheetName = "CHF State Flat File – Full",
                PivotSheetName = "Changes",
                PivotColumnList = new List<FieldTypeAndName>() {
                                    new FieldTypeAndName() {FieldType= "RowFields",ColumnName= "PayerName"},
                                    new FieldTypeAndName() {FieldType= "RowFields",ColumnName= "PlanName"},
                                    new FieldTypeAndName() {FieldType= "RowFields",ColumnName= "Reason_for_Change"},
                                    new FieldTypeAndName() {FieldType= "RowFields",ColumnName= "Reason_for_Change_Details"},
                                    new FieldTypeAndName() {FieldType= "PageFields",ColumnName= "State"},
                                    new FieldTypeAndName() {FieldType= "PageFields",ColumnName= "Change_to_Entry"}                                    
                }
            };

            pivotList.Add(sheet1);            
        }

        public ExcelExportInfo DownloadZipFile(int processId, string clientName, string indication, SqlDataReader reader, string exportFilePath, string folderPath)
        {
            string fileSaveInFolderPath = $"{exportFilePath}/{ clientName + " - " + ProcessTypeName }";
            List<Dictionary<string, object>> dic = null;
            using (var excelPackage = new ExcelPackage())
            {
                ExcelWorksheet userSheet = null;
                bool isAddedExcelSheets = false;
                int sheetIndex = 0;
                while (reader.HasRows || reader.FieldCount > 0)
                {
                    dic = new List<Dictionary<string, object>>();
                    DataTable dtSchema = reader.GetSchemaTable();

                    int rowIndex = 2;
                    if (!isAddedExcelSheets)
                    {
                        // Call Read before accessing data. 
                        while (reader.Read())
                        {
                            if (!isAddedExcelSheets)
                            {
                                Dictionary<string, object> dict = new Dictionary<string, object>();
                                dict = Enumerable.Range(0, reader.FieldCount).ToDictionary(reader.GetName, reader.GetValue);
                                dic.Add(dict);
                            }
                        }
                    }
                    else
                    {
                        List<string> rows = null;
                        var worksheet = excelPackage.Workbook.Worksheets[sheetIndex];
                        worksheet.Cells["A1"].LoadFromDataReader(reader, true);

                        AddExcelHeader(worksheet);

                        SaveExcelFile(excelPackage, fileSaveInFolderPath, "", clientName, indication);
                    }
                    if (!isAddedExcelSheets)
                    {
                        AddExcelWorkSheet(dic, userSheet, excelPackage, ref isAddedExcelSheets);
                    }
                    sheetIndex++;
                    reader.NextResult();
                }

                // Add Pivot Table                
                foreach (var pivotTabel in pivotList)
                {
                    AddPivot_Table(excelPackage, pivotTabel);
                }

                fileSaveInFolderPath = SaveExcelFile(excelPackage, fileSaveInFolderPath, folderPath, clientName, indication);
            }
            //return fileSaveInFolderPath;
            var returnObj = new ExcelExportInfo()
            {
                FilePath = fileSaveInFolderPath,
                ClientName = clientName
            };
            return returnObj;
        }

        void AddExcelHeader(List<Dictionary<string, object>> excelSheets, ExcelWorksheet sheet)
        {
            var excelHeader = new List<ExcelReportFileProperties>();
            var colIndex = 1;
            foreach (KeyValuePair<string, object> headerData in excelSheets[0])
            {
                excelHeader.Add(new ExcelReportFileProperties { HeaderName = headerData.Key, ColumnIndex = colIndex, RowIndex = 1, Width = 40, Height = 30 });
                colIndex++;
            }
            AddHeader(sheet, excelHeader);
        }

        void AddHeader(ExcelWorksheet sheet, IList<ExcelReportFileProperties> headers)
        {
            foreach (var header in headers)
            {

                sheet.Cells[header.RowIndex, header.ColumnIndex].Value = header.HeaderName;
                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.Font.Bold = true;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.WrapText = true;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                sheet.Column(header.ColumnIndex).Width = header.Width;

                sheet.Row(header.RowIndex).Height = header.Height;

            }

        }

        string SaveExcelFile(ExcelPackage excelPackage, string fileSaveInFolderPath, string folderPath, string clientName, string indication)
        {
            try
            {
                // var tempFilePath = Path.GetTempPath(); 
                var tempFilePath = fileSaveInFolderPath;
                if (!string.IsNullOrEmpty(folderPath))
                {
                    //tempFilePath = tempFilePath.Split('\\').ToString();
                    tempFilePath = folderPath;
                }
                else
                {
                    if (!Directory.Exists(tempFilePath))
                        Directory.CreateDirectory(tempFilePath);
                }
                //var filePath = Path.Combine(tempFilePath, ProcessTypeName + (string.IsNullOrEmpty(folderPath) == true ? "" : "-2") + ".xlsx");
                var filePath = Path.Combine(tempFilePath, clientName + "-" + indication + ".xlsx");

                excelPackage.SaveAs(new FileInfo(filePath));
                return tempFilePath;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        void AddDataToSheet(ExcelWorksheet sheet, List<Dictionary<string, object>> dataList)
        {
            for (int i = 0; i < dataList.Count(); i++)
            {
                var rowIndex = i + 2;
                var data = dataList[i];
                int colIndex = 1;
                foreach (var valueData in data)
                {
                    sheet.Cells[rowIndex, colIndex].Value = valueData.Value;
                    colIndex++;
                }

            }
        }

        void AddExcelWorkSheet(List<Dictionary<string, object>> excelSheets, ExcelWorksheet userSheet, ExcelPackage excelPackage, ref bool isAddedExcelSheets)
        {
            foreach (Dictionary<string, object> sheet in excelSheets)
            {
                foreach (KeyValuePair<string, object> sheetName in sheet)
                {
                    userSheet = excelPackage.Workbook.Worksheets.Add(sheetName.Value.ToString());
                    userSheet.OutLineApplyStyle = true;
                }
            }

            isAddedExcelSheets = true;
        }

        string GetProcessNameByProcessId()
        {
            string processTypeName = string.Empty;

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["CustomFileGeneratorDbContext"].ConnectionString))
            {
                connection.Open();
                string strQuery = "select * from process where processId = @processId";
                SqlCommand cmd = new SqlCommand(strQuery, connection);
                cmd.Parameters.AddWithValue("@processId", processId);

                // get query results

                SqlDataReader rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    processTypeName = rdr["ProcessName"].ToString();
                }

                return processTypeName;
            }
        }

        void AddPivot_Table(ExcelPackage excelPackage, HeronOneFileMultipleTabs_Heron_PivotTableViewModel objPivot)
        {
            var userSheet = excelPackage.Workbook.Worksheets[objPivot.PivotSheetName];

            //define the data range on the source sheet
            var worksheetData = excelPackage.Workbook.Worksheets[objPivot.SheetName];
            var dataRange = worksheetData.Cells[worksheetData.Dimension.Address];

            //Clear Pivot sheet Existing Data
            userSheet.Cells[worksheetData.Dimension.Address].Clear();

            //create the pivot table
            var pivotTable = userSheet.PivotTables.Add(userSheet.Cells["A5"], dataRange, "PivotTable");

            //label field
            foreach (var data in objPivot.PivotColumnList.Where(x => x.FieldType == "RowFields"))
            {
                pivotTable.RowFields.Add(pivotTable.Fields[data.ColumnName]);
                pivotTable.DataOnRows = false;
            }

            //Report Filter
            foreach (var data in objPivot.PivotColumnList.Where(x => x.FieldType == "PageFields"))
            {
                pivotTable.PageFields.Add(pivotTable.Fields[data.ColumnName]);
            }

            //data fields
            foreach (var data in objPivot.PivotColumnList.Where(x => x.FieldType == "DataFields"))
            {
                var field = pivotTable.DataFields.Add(pivotTable.Fields[data.ColumnName]);
                field.Function = DataFieldFunctions.Sum;
            }

            //Set tabular form
            (from pf in pivotTable.Fields
             select pf).ToList().ForEach(f =>
             {                 
                 f.Outline = false;
             });
        }

        void AddExcelHeader(ExcelWorksheet worksheet)
        {
            // set autofilter            
            worksheet.Cells[worksheet.Dimension.Address].AutoFilter = true;

            for (int col = 1; col <= worksheet.Dimension.End.Column; col++)
            {
                worksheet.Column(col).Width = 16;
                worksheet.Cells[1, col].Style.Font.Bold = true;
                worksheet.Cells[1, col].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[1, col].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(191, 191, 191));
            }
        }
    }
}
