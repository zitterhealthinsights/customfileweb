﻿using BusinessLayer.ExcelHelper.ClientExcelFiles;
using BusinessLayer.ExcelHelper.Interface;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.ExcelHelper
{
    public class ExcelFileGenerator
    {
        List<IExcelFileInfo> _excelFileGenerator = new List<IExcelFileInfo>();
        public ExcelFileGenerator()
        {
            _excelFileGenerator.Add(new DefaultExcelFile());
            _excelFileGenerator.Add(new RegeneronSanofiCustomFileHCHLRAFiles_RegeneronSanofi());
            _excelFileGenerator.Add(new PayerSciencesHCHLFile_RegeneronSanofi());
            _excelFileGenerator.Add(new McCannHCHLStateFile_RegeneronSanofi());
            _excelFileGenerator.Add(new ProteanHCHLStateFile_RegeneronSanofi());
            _excelFileGenerator.Add(new SandozCustomFile_Sandoz());
            _excelFileGenerator.Add(new HeronOneFileMultipleTabs_Heron());
            _excelFileGenerator.Add(new BeghouOneFileMultipleTabs_Regeneron());
            _excelFileGenerator.Add(new WeeklyFilesforNovartisEntresto_Novartis());
            _excelFileGenerator.Add(new MonthlyFileforNovartisRCC_Novartis());
            _excelFileGenerator.Add(new MonthlyFilesforBMS_BMS());
            _excelFileGenerator.Add(new WeeklyFilesforNovartisJadenu_Novartis());
            _excelFileGenerator.Add(new MonthlyFilesforGenentech_Genentech());
            _excelFileGenerator.Add(new MonthlyFilesforNovartisAccessAnalysis_Novartis());
            _excelFileGenerator.Add(new GenentechSpecialtyPharmacy_Genentech());
            _excelFileGenerator.Add(new MonthlyGenentechOcularFile_Genentech());
            _excelFileGenerator.Add(new MonthlyFilesforIpsenDysport_Ipsen());
        }

        public ExcelExportInfo GenerateExcel(int processId,string clientName,string indication, SqlDataReader response, string exportFilePath, string folderPath)
        {
            var objExcelFile = _excelFileGenerator.Find(x => x.ProcessId == processId);
            if (objExcelFile == null)
            {
                var excelGenerator = _excelFileGenerator.Find(x => x.ProcessId == 0);
                return excelGenerator.DownloadZipFile(processId, clientName, indication, response, exportFilePath, folderPath);
            }
            return objExcelFile.DownloadZipFile(processId, clientName, indication, response, exportFilePath, folderPath);
        }
    }
}
