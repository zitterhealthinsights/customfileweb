﻿using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.ExcelHelper.Interface
{
    public interface IExcelFileInfo
    {
        int ProcessId { get; }

        string ProcessTypeName { get; }

        //string DownloadZipFile(int processId, List<List<Dictionary<string, object>>> response, string exportFilePath, string folderPath);

        ExcelExportInfo DownloadZipFile(int processId,string clientName, string indication, SqlDataReader response, string exportFilePath, string folderPath);
    }
}
