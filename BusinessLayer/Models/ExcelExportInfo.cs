﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class ExcelExportInfo
    {
        public string FilePath { get; set; }
        public string ClientName { get; set; }
    }
}
