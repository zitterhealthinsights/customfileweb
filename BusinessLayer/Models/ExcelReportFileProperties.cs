﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class ExcelReportFileProperties
    {
        public string HeaderName { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }

        public int RowIndex { get; set; }

        public int ColumnIndex { get; set; }

    }
}
