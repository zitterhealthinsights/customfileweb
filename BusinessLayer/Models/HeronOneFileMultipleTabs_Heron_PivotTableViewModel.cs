﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class HeronOneFileMultipleTabs_Heron_PivotTableViewModel
    {
        public string SheetName { get; set; }
        public string PivotSheetName { get; set; }
        public List<FieldTypeAndName> PivotColumnList { get; set; }
    }


    public class FieldTypeAndName
    {
        public string FieldType { get; set; }
        public string ColumnName { get; set; }
    }
}
