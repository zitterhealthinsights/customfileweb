﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Repository
{
    public class AdoQueryExecuter<T>
    {
        public DataTable Get(string query)
        {
            DataTable table = new DataTable();
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["SuravyRefDbContext"].ConnectionString;
                SqlDataAdapter da = new SqlDataAdapter(query, connectionString);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da.Fill(ds);
                table = ds.Tables[0];
                return table;
            }
            catch (Exception ex)
            {

            }

            return table;
        }

        public List<DataTable> GetByParam(string query, int param, string parameterName)
        {
            List<DataTable> table = new List<DataTable>();
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["SuravyRefDbContext"].ConnectionString;
                SqlDataAdapter da = new SqlDataAdapter(query, connectionString);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add($"@{parameterName}", SqlDbType.Int).Value = param;
                DataSet ds = new DataSet();
                da.Fill(ds);
                for (int i = 0; i <= ds.Tables.Count; i++)
                {
                    table.Add(ds.Tables[i]);
                }
                return table;
            }
            catch (Exception ex)
            {

            }

            return table;
        }

        public DataTable GetByParam(string connection, string query, SqlParameter[] param)
        {
            DataTable table = new DataTable();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(query, connection);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                foreach (var parameter in param)
                {
                    da.SelectCommand.Parameters.Add(parameter.ParameterName, parameter.DbType).Value = parameter.SourceColumn;
                }
                DataSet ds = new DataSet();
                da.Fill(ds);
                table = ds.Tables[0];
                return table;
            }
            catch (Exception ex)
            {

            }

            return table;
        }

        public List<DataTable> GetAllByParam(string connection, string query, SqlParameter[] param)
        {
            List<DataTable> table = new List<DataTable>();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(query, connection);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                foreach (var parameter in param)
                {
                    da.SelectCommand.Parameters.Add(parameter.ParameterName, parameter.DbType).Value = parameter.SourceColumn;
                }
                //The time in seconds to wait for the command to execute.The default is 30 seconds
                da.SelectCommand.CommandTimeout = 1800;

                List<Dictionary<string, string>> dataSet = new List<Dictionary<string, string>>();



                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables.Count > 1)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        table.Add(ds.Tables[i]);
                    }
                }
                else
                {
                    table.Add(ds.Tables[0]);
                }
                return table;
            }
            catch (Exception ex)
            {

            }

            return table;
        }

        //public List<List<Dictionary<string, object>>> GetAllByParameter(string connection, string query, SqlParameter[] param)
        //{
        //    List<Dictionary<string, object>> dic = null;
        //    List<List<Dictionary<string, object>>> tbl = new List<List<Dictionary<string, object>>>();

        //    using (SqlConnection con = new SqlConnection(connection))
        //    {
        //        con.Open();
        //        using (SqlCommand cmd = new SqlCommand(query, con))
        //        {
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            foreach (var parameter in param)
        //            {
        //                cmd.Parameters.Add(parameter.ParameterName, parameter.DbType).Value = parameter.SourceColumn;
        //            }
        //            //The time in seconds to wait for the command to execute.The default is 30 seconds
        //            cmd.CommandTimeout = 1800;

        //            SqlDataReader reader = cmd.ExecuteReader();

        //            while (reader.HasRows)
        //            {
        //                dic = new List<Dictionary<string, object>>();
        //                // Call Read before accessing data. 
        //                while (reader.Read())
        //                {
        //                    Dictionary<string, object> dict = new Dictionary<string, object>();
        //                    //for (int lp = 0; lp < reader.FieldCount; lp++)
        //                    //{
        //                    //    dict.Add(reader.GetName(lp), reader.GetValue(lp));
        //                    //}

        //                    dict = Enumerable.Range(0, reader.FieldCount).ToDictionary(reader.GetName, reader.GetValue);
        //                    dic.Add(dict);
        //                }
        //                tbl.Add(dic);
        //                reader.NextResult();
        //            }
        //        }
        //    }
        //    return tbl;
        //}

        public List<DataTable> GetData(string connection, string query, SqlParameter[] param)
        {
            List<DataTable> table = new List<DataTable>();
            List<Dictionary<string, string>> tbl = new List<Dictionary<string, string>>();

            //DataTable ResultsData = new DataTable();

            using (SqlConnection con = new SqlConnection(connection))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    foreach (var parameter in param)
                    {
                        cmd.Parameters.Add(parameter.ParameterName, parameter.DbType).Value = parameter.SourceColumn;
                    }
                    //The time in seconds to wait for the command to execute.The default is 30 seconds
                    cmd.CommandTimeout = 1800;

                    SqlDataReader reader = cmd.ExecuteReader();

                    int c = 0;
                    bool firstTime = true;
                    while (reader.HasRows)
                    {
                        DataTable ResultsData = new DataTable();
                        //Get the Columns names, types, this will help
                        //when we need to format the cells in the excel sheet.
                        DataTable dtSchema = reader.GetSchemaTable();
                        var listCols = new List<DataColumn>();
                        if (dtSchema != null)
                        {
                            foreach (DataRow drow in dtSchema.Rows)
                            {
                                string columnName = Convert.ToString(drow["ColumnName"]);
                                var column = new DataColumn(columnName, (Type)(drow["DataType"]));
                                column.Unique = (bool)drow["IsUnique"];
                                column.AllowDBNull = (bool)drow["AllowDBNull"];
                                column.AutoIncrement = (bool)drow["IsAutoIncrement"];
                                listCols.Add(column);
                                ResultsData.Columns.Add(column);
                            }
                        }

                        // Call Read before accessing data. 
                        while (reader.Read())
                        {
                            DataRow dataRow = ResultsData.NewRow();
                            for (int i = 0; i < listCols.Count; i++)
                            {
                                dataRow[(listCols[i])] = reader[i];
                            }
                            ResultsData.Rows.Add(dataRow);
                            c++;

                        }
                        if (ResultsData.Rows.Count > 0)
                        {
                            // ExportToOxml(firstTime);
                            //ResultsData.Clear();
                        }
                        table.Add(ResultsData);
                        reader.NextResult();
                    }
                }
            }
            return table;
        }

        public async Task<SqlDataReader> GetAllByParameter(string connection, string query, SqlParameter[] param)
        {
            SqlDataReader reader = null;

            var con = new SqlConnection(connection);
            con.Open();
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandType = CommandType.StoredProcedure;
            foreach (var parameter in param)
            {
                cmd.Parameters.Add(parameter.ParameterName, parameter.DbType).Value = parameter.SourceColumn;
            }
            //The time in seconds to wait for the command to execute.The default is 30 seconds
            cmd.CommandTimeout = 1800;
            reader = cmd.ExecuteReader();
            return reader;
        }
    }
}
