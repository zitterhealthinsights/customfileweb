﻿using BusinessLayer.Models;
using BusinessLayer.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessLayer.Services
{
    public class CustomFileGeneratorServices
    {
        IRepository<DataTable> repo = new Repository<DataTable>();
        static string connectionString = ConfigurationManager.ConnectionStrings["CustomFileGeneratorDbContext"].ConnectionString;

        public DataTable GetClientList()
        {
            SqlParameter[] paramList = {new SqlParameter("@ClientName",SqlDbType.VarChar,150,"<ALL>"),
                        };
            return repo.GetAll(connectionString, "usp_CF_Get_ClientList", paramList);
        }

        public DataTable GetScheduleList()
        {
            SqlParameter[] paramList = { };
            return repo.GetAll(connectionString, "usp_CF_Get_ScheduleTypesList", paramList);
        }

        public DataTable GetProcessList(string clientName, string scheduleType)
        {
            SqlParameter[] paramList = {new SqlParameter("@ClientName",SqlDbType.VarChar,150,clientName),
                                        new SqlParameter("@ScheduleTypeName",SqlDbType.VarChar,255,scheduleType)

            };
            return repo.GetAll(connectionString, "usp_CF_Get_ProcessList", paramList);
        }

        public async Task<ExcelExportInfo> GetProcessStepByProcessId(int processId)
        {
            string returnFilePath = string.Empty;
            SqlParameter[] paramList = {new SqlParameter("@ProcessID",SqlDbType.Int,0, Convert.ToString(processId))

            };
            DataTable dbProcessSteps = repo.GetAll(connectionString, "usp_CF_Get_ProcessSteps_ByProcessId", paramList);
            var objProcessDetail = ConvertDataTableToDictionaryList(dbProcessSteps);

            ExcelExportInfo retunrObj = null;
            foreach (var data in objProcessDetail)
            {
                retunrObj = new ExcelExportInfo();

                string clientId = string.Empty;
                data.TryGetValue("ClientID", out clientId);

                string clientName = string.Empty;
                data.TryGetValue("ClientName", out clientName);

                string categoryId = string.Empty;
                data.TryGetValue("CategoryID", out categoryId);

                string indication = string.Empty;
                data.TryGetValue("Indication", out indication);

                string scheduleTypeID = string.Empty;
                data.TryGetValue("ScheduleTypeID", out scheduleTypeID);

                string exportFilePath = string.Empty;
                data.TryGetValue("ExportFilePath", out exportFilePath);

                string folderPath = returnFilePath;
                retunrObj = await GetCustomFileData(Convert.ToInt32(clientId), clientName, categoryId, indication, string.Empty, Convert.ToInt32(scheduleTypeID), exportFilePath, processId, folderPath);

            }

            return retunrObj;
        }

        public async Task<ExcelExportInfo> GetCustomFileData(int clientId, string clientName,string categoryId, string indication, string drugArray, int scheduleTypeID, string exportFilePath, int processId, string folderPath)
        {
            SqlParameter[] paramList = {new SqlParameter("@ClientID",SqlDbType.Int,0, Convert.ToString(clientId)),
                new SqlParameter("@CategoryID",SqlDbType.Int,0, Convert.ToString(categoryId)),
                new SqlParameter("@IndicationArray",SqlDbType.NVarChar,255, indication),
                new SqlParameter("@DrugArray",SqlDbType.NVarChar,255, string.Empty),
                new SqlParameter("@ScheduleTypeID",SqlDbType.Int,0, Convert.ToString( scheduleTypeID)),

            };

            var dbCustomFileDataList = await repo.GetAllByParam(connectionString, "usp_CF_CustomFile", paramList);

            //if (dbCustomFileDataList.Count > 0)
            if (dbCustomFileDataList != null)
            {
                //var customFileList = ExtensionMethod.DataTableToDictionary(dbCustomFileDataList);

                //var customFileList = ConvertDataTableToListOfDictionary(dbCustomFileDataList);

                BusinessLayer.ExcelHelper.ExcelFileGenerator objExcelGenerator = new ExcelHelper.ExcelFileGenerator();
                var result = objExcelGenerator.GenerateExcel(processId, clientName, indication, dbCustomFileDataList, exportFilePath, folderPath);

                return result;
            }
            return new ExcelExportInfo();

        }

        private List<List<Dictionary<string, string>>> ConvertDataTableToListOfDictionary(List<DataTable> dt)
        {
            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(dt);
            List<List<Dictionary<string, string>>> excelList = new List<List<Dictionary<string, string>>>();
            excelList = JsonConvert.DeserializeObject<List<List<Dictionary<string, string>>>>(JSONresult);
            return excelList;
        }

        private List<Dictionary<string, string>> ConvertDataTableToDictionaryList(DataTable dt)
        {
            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(dt);
            List<Dictionary<string, string>> excelList = new List<Dictionary<string, string>>();
            excelList = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(JSONresult);
            return excelList;
        }

        private Dictionary<string, string> ConvertDataTableToDictionary(DataTable dt)
        {
            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(dt);
            Dictionary<string, string> excelList = new Dictionary<string, string>();
            excelList = JsonConvert.DeserializeObject<Dictionary<string, string>>(JSONresult);
            return excelList;
        }

        public string DataTableToJSONWithJavaScriptSerializer(List<DataTable> tableList)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataTable table in tableList)
            {
                foreach (DataRow row in table.Rows)
                {
                    childRow = new Dictionary<string, object>();
                    foreach (DataColumn col in table.Columns)
                    {
                        childRow.Add(col.ColumnName, row[col]);
                    }
                    parentRow.Add(childRow);
                }
            }
            return jsSerializer.Serialize(parentRow);
        }

    }

    public static class ExtensionMethod
    {
        public static List<List<Dictionary<string, object>>> DataTableToDictionary(this List<DataTable> dataTableList)
        {
            List<Dictionary<string, object>> dictionaries = null;

            List<List<Dictionary<string, object>>> dictionariesList = new List<List<Dictionary<string, object>>>();

            foreach (DataTable dt in dataTableList)
            {
                dictionaries = new List<Dictionary<string, object>>();

                foreach (DataRow row in dt.Rows)
                {
                    Dictionary<string, object> dictionary = Enumerable.Range(0, dt.Columns.Count)
                        .ToDictionary(i => dt.Columns[i].ColumnName, i => row.ItemArray[i]);



                    dictionaries.Add(dictionary);
                }
                dictionariesList.Add(dictionaries);
            }
            return dictionariesList;
        }
    }
}
