﻿using Newtonsoft.Json;
using POCAPI.Service;
using System;
using BusinessLayer;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;

namespace POCAPI.Controllers
{
    [RoutePrefix("api/CustomFileGenerator")]
    public class CustomFileGeneratorController : ApiController
    {
        private readonly BusinessLayer.Services.CustomFileGeneratorServices _customFileGeneratorServices;

        public CustomFileGeneratorController()
        {
            _customFileGeneratorServices = new BusinessLayer.Services.CustomFileGeneratorServices();
        }

        [Route("GetClientList")]
        [HttpGet]
        public IHttpActionResult GetClientList()
        {
            try
            {
                //CustomFileGeneratorServices service = new CustomFileGeneratorServices();
                //var drugs = service.GetClientList();
                var clientList = _customFileGeneratorServices.GetClientList();
                return Content(HttpStatusCode.OK, clientList);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, "Internal Server Error");
            }
        }

        [Route("GetScheduleList")]
        [HttpGet]
        public IHttpActionResult GetScheduleList()
        {
            try
            {
                //CustomFileGeneratorServices service = new CustomFileGeneratorServices();
                var dbScheduleList = _customFileGeneratorServices.GetScheduleList();
                return Content(HttpStatusCode.OK, dbScheduleList);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, "Internal Server Error");
            }
        }

        [Route("GetProcessList")]
        [HttpGet]
        public IHttpActionResult GetProcessList(string clientName, string scheduleType)
        {
            try
            {
                //CustomFileGeneratorServices service = new CustomFileGeneratorServices();
                var dbProcessList = _customFileGeneratorServices.GetProcessList(clientName, scheduleType);
                return Content(HttpStatusCode.OK, dbProcessList);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, "Internal Server Error");
            }
        }

        [Route("GetProcessStepByProcessId")]
        [HttpGet]
        public IHttpActionResult GetProcessStepByProcessId(int processId)
        {
            try
            {
                CustomFileGeneratorServices service = new CustomFileGeneratorServices();
                var dbProcess = service.GetProcessStepByProcessId(processId);
                return Content(HttpStatusCode.OK, dbProcess);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, "Internal Server Error");
            }
        }

        [Route("GetCustomFileData")]
        [HttpGet]
        public IHttpActionResult GetCustomFileData(int clientId, string categoryId, string indication, string drugArray, int scheduleTypeID)
        {
            try
            {
                CustomFileGeneratorServices service = new CustomFileGeneratorServices();
                var dbProcess = service.GetCustomFileData(clientId, categoryId, indication, drugArray, scheduleTypeID);
                return Content(HttpStatusCode.OK, dbProcess);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, "Internal Server Error");
            }
        }

        [Route("GenerateCustomExcelFile")]
        [HttpGet]
        public async Task<IHttpActionResult> GenerateCustomExcelFile(int processId)
        {
            try
            {
                var result = await _customFileGeneratorServices.GetProcessStepByProcessId(processId);               
                return Content(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, "Internal Server Error");
            }
        }
    }
}
