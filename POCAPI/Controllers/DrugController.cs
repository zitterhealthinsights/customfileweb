﻿using POCAPI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POCAPI.Controllers
{
    [RoutePrefix("api/Drug")]
    public class DrugController : ApiController
    {
        [Route("GetAllDrug")]
        [HttpGet]
        public IHttpActionResult GetAllDrug()
        {
            try
            {
                DrugService service = new DrugService();
                var drugs = service.GetAllDrug();
                return Content(HttpStatusCode.OK, drugs);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, "Internal Server Error");
            }
        }
    }
}
