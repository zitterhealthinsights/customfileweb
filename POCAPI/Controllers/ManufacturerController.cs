﻿using POCAPI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POCAPI.Controllers
{
    [RoutePrefix("api/Manufacturer")]
    public class ManufacturerController : ApiController
    {
        [Route("GetAllManufacturer")]
        public IHttpActionResult GetAllManufacturer()
        {
            try
            {
                ManufacturerService service = new ManufacturerService();
                return Ok(service.GetMenufacturer());
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

        [Route("GetAllManufacturerWithPivot")]
        public IHttpActionResult GetAllManufacturerWithPivot()
        {
            try
            {
                ManufacturerService service = new ManufacturerService();
                return Ok(service.GetMenufacturerWithPivot());
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }
    }
}
