﻿using POCAPI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POCAPI.Controllers
{
    [RoutePrefix("api/TherapArea")]
    public class TherapAreaController : ApiController
    {
        [Route("GetAll")]
        public IHttpActionResult GetAll()
        {
            try
            {
                TherapAreaService therapAreaService = new TherapAreaService();
                var therapData = therapAreaService.GetAll();
                return Ok(therapData);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }
    }
}
