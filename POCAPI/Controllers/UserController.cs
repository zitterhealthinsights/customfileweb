﻿using POCAPI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POCAPI.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {

        [Route("GetUserDetailById")]
        [HttpGet]
        public IHttpActionResult GetUserDetailById(int userId)
        {
            try
            {
                UserService service = new UserService();
                var userDetail = service.GetUserDetail(userId);
                return Content(HttpStatusCode.OK, userDetail);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, "Internal Server Error");
            }
        }

        [Route("GetUserOrderDetailById")]
        [HttpGet]
        public IHttpActionResult GetUserOrderDetailById(int customerOrderId)
        {
            try
            {
                UserService service = new UserService();
                var userDetail = service.GetUserOrderDetailById(customerOrderId);
                return Content(HttpStatusCode.OK, userDetail);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, "Internal Server Error");
            }
        }
    }
}
