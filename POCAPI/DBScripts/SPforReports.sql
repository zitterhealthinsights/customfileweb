﻿USE [SURVEY_REF]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDrugsForReport]    Script Date: 1/2/2018 9:18:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Manjinder Singh
-- Create date: 12/26/2017
-- Description:	
-- =============================================
/*
EXEC [usp_GetDrugsForReport] 
WITH RECOMPILE

*/
CREATE PROCEDURE [dbo].[usp_GetDrugsForReport]
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		--Associated with Drug
		--SELECT DrugID,DrugName,Active,GenericName,
		--NoCop,NoPAP,NoFDN,NoFTO,NoBridge,Discontinued,
		--convert(varchar(20), LastUpdated, 120) as LastUpdated,LastUpdatedBy,ManufacturerID FROM Drug

		SELECT DrugID,DrugName FROM Drug

	END TRY

	BEGIN CATCH
		DECLARE @Err_Message VARCHAR(MAX)

		SELECT @Err_Message = ERROR_MESSAGE()

		RAISERROR (
				'Error in SP: usp_GetDrugsForReport, %s'
				,15
				,1
				,@Err_Message
				)
	END CATCH
END


GO
/****** Object:  StoredProcedure [dbo].[usp_GetManufacturerForReport]    Script Date: 1/2/2018 9:18:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Manjinder Singh
-- Create date: 12/26/2017
-- Description:	
-- =============================================
/*
EXEC [usp_GetManufacturerForReport] 
WITH RECOMPILE

*/
CREATE PROCEDURE [dbo].[usp_GetManufacturerForReport]
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		--Associated with Drug
		--SELECT ManufacturerID,ManufacturerName,convert(varchar(20), LastUpdated, 120) as LastUpdated,LastUpdatedBy,Active FROM Manufacturer
		SELECT ManufacturerID,ManufacturerName FROM Manufacturer
	END TRY

	BEGIN CATCH
		DECLARE @Err_Message VARCHAR(MAX)

		SELECT @Err_Message = ERROR_MESSAGE()

		RAISERROR (
				'Error in SP: usp_GetManufacturerForReport, %s'
				,15
				,1
				,@Err_Message
				)
	END CATCH
END


GO
/****** Object:  StoredProcedure [dbo].[usp_GetManufacturerTAPivotForReport]    Script Date: 1/2/2018 9:18:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Manjinder Singh
-- Create date: 12/26/2017
-- Description:	
-- =============================================
/*
EXEC [usp_GetManufacturerTAPivotForReport] 
WITH RECOMPILE

*/
CREATE PROCEDURE [dbo].[usp_GetManufacturerTAPivotForReport]
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		select M.ManufacturerName,TA.TherapAreaName from TherapArea TA INNER JOIN TherapAreaDrug TAD ON
TA.TherapAreaID=TAD.TherapAreaID
INNER JOIN Drug D ON
D.DrugID=TAD.DrugID 
INNER JOIN ManufacturerDrug MD ON
MD.DrugID=D.DrugID 
INNER JOIN Manufacturer M ON
M.ManufacturerID=MD.ManufacturerID order by m.ManufacturerName asc
	END TRY

	BEGIN CATCH
		DECLARE @Err_Message VARCHAR(MAX)

		SELECT @Err_Message = ERROR_MESSAGE()

		RAISERROR (
				'Error in SP: usp_GetManufacturerTAPivotForReport, %s'
				,15
				,1
				,@Err_Message
				)
	END CATCH
END


GO
/****** Object:  StoredProcedure [dbo].[usp_GetTherapAreaForReport]    Script Date: 1/2/2018 9:18:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Manjinder Singh
-- Create date: 12/26/2017
-- Description:	
-- =============================================
/*
EXEC [usp_GetTherapAreaForReport] 
WITH RECOMPILE

*/
CREATE PROCEDURE [dbo].[usp_GetTherapAreaForReport]
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		--Associated with Drug
		--SELECT TherapAreaID,TherapAreaName,Active,Priority,Scheduling,convert(varchar(20), LastUpdated, 120) as LastUpdated,LastUpdatedBy FROM TherapArea
		SELECT TherapAreaID,TherapAreaName FROM TherapArea
	END TRY

	BEGIN CATCH
		DECLARE @Err_Message VARCHAR(MAX)

		SELECT @Err_Message = ERROR_MESSAGE()

		RAISERROR (
				'Error in SP: usp_GetTherapAreaForReport, %s'
				,15
				,1
				,@Err_Message
				)
	END CATCH
END


GO
