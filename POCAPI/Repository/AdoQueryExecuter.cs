﻿using POCAPI.CommonUtill;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace POCAPI.Repository
{
    public class AdoQueryExecuter<T>
    {
        public DataTable Get(string query)
        {
            DataTable table = new DataTable();
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["SuravyRefDbContext"].ConnectionString;
                SqlDataAdapter da = new SqlDataAdapter(query, connectionString);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da.Fill(ds);
                table = ds.Tables[0];
                return table;
            }
            catch (Exception ex)
            {

            }

            return table;
        }

        public List<DataTable> GetByParam(string query, int param, string parameterName)
        {
            List<DataTable> table = new List<DataTable>();
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["SuravyRefDbContext"].ConnectionString;
                SqlDataAdapter da = new SqlDataAdapter(query, connectionString);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add($"@{parameterName}", SqlDbType.Int).Value = param;
                DataSet ds = new DataSet();
                da.Fill(ds);
                for (int i = 0; i <= ds.Tables.Count; i++)
                {
                    table.Add(ds.Tables[i]);
                }
                return table;
            }
            catch (Exception ex)
            {

            }

            return table;
        }

        public DataTable GetByParam(string connection, string query, SqlParameter[] param)
        {
            DataTable table = new DataTable();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(query, connection);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                foreach (var parameter in param)
                {
                    da.SelectCommand.Parameters.Add(parameter.ParameterName, parameter.DbType).Value = parameter.SourceColumn;
                }
                DataSet ds = new DataSet();
                da.Fill(ds);
                table = ds.Tables[0];
                return table;
            }
            catch (Exception ex)
            {

            }

            return table;
        }

        public List<DataTable> GetAllByParam(string connection, string query, SqlParameter[] param)
        {
            List<DataTable> table = new List<DataTable>();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(query, connection);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                foreach (var parameter in param)
                {
                    da.SelectCommand.Parameters.Add(parameter.ParameterName, parameter.DbType).Value = parameter.SourceColumn;
                }

                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables.Count > 1)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        table.Add(ds.Tables[i]);
                    }
                }
                else
                {
                    table.Add(ds.Tables[0]);
                }
                return table;
            }
            catch (Exception ex)
            {

            }

            return table;
        }

    }
}