﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace POCAPI.Repository
{
    public interface IRepository<T>
    {


        DataTable GetAll(string query);

        List<DataTable> GetAllByParameter(string query, int param, string parameterName);

        DataTable GetAll(string connection, string query, SqlParameter[] param);

        List<DataTable> GetAllByParameter(string connection, string query, SqlParameter[] param);

        IEnumerable<T> GetByCondition(Expression<Func<T, bool>> condition);

        T Get(Expression<Func<T, bool>> condition);

    }
}