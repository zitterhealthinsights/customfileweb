﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace POCAPI.Repository
{
    public class Repository<T> : IRepository<T> where T : DataTable
    {
        AdoQueryExecuter<T> adoQueryAdapter;

        public Repository()
        {
            adoQueryAdapter = new AdoQueryExecuter<T>();
        }

        public T Get(Expression<Func<T, bool>> condition)
        {
            throw new NotImplementedException();
        }

        public DataTable GetAll(string query)
        {
            return adoQueryAdapter.Get(query);
        }

        public IEnumerable<T> GetByCondition(Expression<Func<T, bool>> condition)
        {
            throw new NotImplementedException();
        }

        public List<DataTable> GetAllByParameter(string query, int param, string parameterName)
        {
            return adoQueryAdapter.GetByParam(query, param, parameterName);
        }

        public DataTable GetAll(string connection, string query, SqlParameter[] param)
        {
            return adoQueryAdapter.GetByParam(connection, query, param);
        }

        public List<DataTable> GetAllByParameter(string connection, string query, SqlParameter[] param)
        {
            return adoQueryAdapter.GetAllByParam(connection, query, param);
        }
    }
}