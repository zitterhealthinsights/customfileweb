﻿using POCAPI.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace POCAPI.Service
{
    public class CustomFileGeneratorServices
    {
        IRepository<DataTable> repo = new Repository<DataTable>();
        static string connectionString = ConfigurationManager.ConnectionStrings["CustomFileGeneratorDbContext"].ConnectionString;

        public DataTable GetClientList()
        {
            SqlParameter[] paramList = {new SqlParameter("@ClientName",SqlDbType.VarChar,150,"<ALL>"),
                        };
            return repo.GetAll(connectionString, "usp_CF_Get_ClientList", paramList);
        }

        public DataTable GetScheduleList()
        {
            SqlParameter[] paramList = { };
            return repo.GetAll(connectionString, "usp_CF_Get_ScheduleTypesList", paramList);
        }

        public DataTable GetProcessList(string clientName, string scheduleType)
        {
            SqlParameter[] paramList = {new SqlParameter("@ClientName",SqlDbType.VarChar,150,clientName),
                                        new SqlParameter("@ScheduleTypeName",SqlDbType.VarChar,255,scheduleType)

            };
            return repo.GetAll(connectionString, "usp_CF_Get_ProcessList", paramList);
        }

        public DataTable GetProcessStepByProcessId(int processId)
        {
            SqlParameter[] paramList = {new SqlParameter("@ProcessID",SqlDbType.Int,0, Convert.ToString(processId))

            };
            return repo.GetAll(connectionString, "usp_CF_Get_ProcessSteps_ByProcessId", paramList);
        }

        public List<DataTable> GetCustomFileData(int clientId, string categoryId, string indication, string drugArray, int scheduleTypeID)
        {
            SqlParameter[] paramList = {new SqlParameter("@ClientID",SqlDbType.Int,0, Convert.ToString(clientId)),
                new SqlParameter("@CategoryID",SqlDbType.Int,0, Convert.ToString(categoryId)),
                new SqlParameter("@IndicationArray",SqlDbType.NVarChar,255, indication),
                new SqlParameter("@DrugArray",SqlDbType.NVarChar,255, string.Empty),
                new SqlParameter("@ScheduleTypeID",SqlDbType.Int,0, Convert.ToString( scheduleTypeID)),

            };
            return repo.GetAllByParameter(connectionString, "usp_CF_CustomFile", paramList);
        }
    }
}