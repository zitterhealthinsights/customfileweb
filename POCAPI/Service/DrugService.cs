﻿using POCAPI.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace POCAPI.Service
{
    public class DrugService
    {
        IRepository<DataTable> repo = new Repository<DataTable>();

        string query = "usp_GetDrugsForReport";

        public DataTable GetAllDrug()
        {
            return repo.GetAll(query);
        }
    }
}