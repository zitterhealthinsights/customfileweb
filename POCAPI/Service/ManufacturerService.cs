﻿using POCAPI.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace POCAPI.Service
{
    public class ManufacturerService
    {
        IRepository<DataTable> repository = new Repository<DataTable>();
        string query = "usp_GetManufacturerForReport";

        public DataTable GetMenufacturer()
        {
            return repository.GetAll(query);
        }


        public DataTable GetMenufacturerWithPivot()
        {
            return repository.GetAll("usp_GetManufacturerTAPivotForReport");
        }
    }
}