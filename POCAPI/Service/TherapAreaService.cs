﻿using POCAPI.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace POCAPI.Service
{
    public class TherapAreaService
    {
        IRepository<DataTable> repository = new Repository<DataTable>();

        string query = "usp_GetTherapAreaForReport";

        public DataTable GetAll()
        {
            return repository.GetAll(query);
        }
    }
}