﻿using POCAPI.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace POCAPI.Service
{
    public class UserService
    {
        IRepository<DataTable> repo = new Repository<DataTable>();

        string query = "usp_GetCustomerOrderForReport";

        public List<DataTable> GetUserDetail(int userId)
        {
            return repo.GetAllByParameter(query, userId, "CustomerId");
        }

        public  List<DataTable> GetUserOrderDetailById(int customerOrderId)
        {
            return repo.GetAllByParameter("usp_GetCustomerOrderDetailForReport", customerOrderId, "customerOrderId");
        }
    }
}