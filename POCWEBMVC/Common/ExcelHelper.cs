﻿using BusinessLayer.Models;
using Ionic.Zip;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table.PivotTable;
using POCWEBMVC.ExcelExtension;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace POCWEBMVC.Common
{
    public class ExcelHelper
    {
        private static string baseAPIURL = ConfigurationManager.AppSettings["BaseAPIURL"];

        /// <summary>
        /// Method to generate Excel file
        /// </summary>
        /// <param name="urlList">Url List to get data in json format</param>
        /// <param name="sheet">List of Sheet on Excel</param>
        /// <param name="header">List of header of each sheet in excel</param>
        /// <returns></returns>
        public string GenerateExcelFromJSON(List<string> urlList, List<string> sheetList, List<List<string>> header)
        {
            string path = string.Empty;
            try
            {

                using (var client = new HttpClient())
                {
                    List<List<Dictionary<string, string>>> response = new List<List<Dictionary<string, string>>>();
                    foreach (string url in urlList)
                    {
                        var responseData = client.GetAsync(url).Result;
                        if (responseData.StatusCode == HttpStatusCode.OK)
                        {
                            var responseJson = responseData.Content.ReadAsStringAsync().Result;
                            var data = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(responseJson);
                            response.Add(data);
                        }
                    }
                    path = GetExcel(response, sheetList, header);
                }
            }
            catch (Exception ex)
            {
            }
            return path;
        }

        public string GetExcel(List<List<Dictionary<string, string>>> response, List<string> sheetList, List<List<string>> headerList)
        {
            using (var excelPackage = new ExcelPackage())
            {

                int sheetIndex = 0;
                foreach (string sheet in sheetList)
                {
                    ExcelWorksheet userSheet = excelPackage.Workbook.Worksheets.Add(sheet);
                    userSheet.OutLineApplyStyle = true;

                    var colIndex = 1;
                    var excelHeader = new List<ExcelReportFileProperties>();
                    foreach (var header in headerList[sheetIndex])
                    {
                        excelHeader.Add(new ExcelReportFileProperties { HeaderName = header, ColumnIndex = colIndex, RowIndex = 1, Width = 40, Height = 30 });
                        colIndex++;
                    }

                    if (sheet == "PivotTable")
                        //AddPivotTable(sheetList, userSheet, response[sheetIndex]);
                        AddPivot_Table(sheetList, userSheet, response[sheetIndex], excelPackage);

                    AddHeader(userSheet, excelHeader);

                    if (sheet != "PivotTable")
                        AddDataToSheet(userSheet, response[sheetIndex]);

                    sheetIndex++;

                }
                var downloadPath = Save(excelPackage);
                return downloadPath;
            }


        }

        /// <summary>
        /// Method save sheet after adding header and data to sheet
        /// </summary>
        /// <param name="excelPackage"></param>
        /// <returns></returns>
        public string Save(ExcelPackage excelPackage)
        {
            try
            {
                var tempFilePath = Path.GetTempPath();
                var currentPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data");
                //if (!Directory.Exists(currentPath))
                //    Directory.CreateDirectory(currentPath);

                var filePath = Path.Combine(tempFilePath, DateTime.Now.ToFileTime().ToString() + ".xlsx");

                excelPackage.SaveAs(new FileInfo(filePath));
                return filePath;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }


        }

        /// <summary>
        /// This method set header data to excel sheet
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="headers"></param>
        void AddHeader(ExcelWorksheet sheet, IList<ExcelReportFileProperties> headers)
        {
            foreach (var header in headers)
            {

                sheet.Cells[header.RowIndex, header.ColumnIndex].Value = header.HeaderName;
                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.Font.Bold = true;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.WrapText = true;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                sheet.Column(header.ColumnIndex).Width = header.Width;

                sheet.Row(header.RowIndex).Height = header.Height;

            }

        }

        /// <summary>
        /// This method add data to sheet
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="dataList"></param>
        void AddDataToSheet(ExcelWorksheet sheet, List<Dictionary<string, string>> dataList)
        {
            for (int i = 0; i < dataList.Count(); i++)
            {
                var rowIndex = i + 2;
                var data = dataList[i];
                int colIndex = 1;
                foreach (var valueData in data)
                {
                    sheet.Cells[rowIndex, colIndex].Value = valueData.Value;
                    colIndex++;
                }

            }
        }

        /// <summary>
        /// Method to add Pivot table
        /// </summary>
        /// <param name="sheetList"></param>
        /// <param name="sheet"></param>
        /// <param name="dataList"></param>
        public void AddPivotTable(List<string> sheetList, ExcelWorksheet sheet, List<Dictionary<string, string>> dataList)
        {
            var previousManufacturer = string.Empty;
            var formulaStartRowIndex = 0;
            int pivotRowIndex = 1;
            int pivotColIndex = 1;
            for (int i = 0; i < dataList.Count(); i++)
            {
                var rowIndex = i + 2;
                var data = dataList[i];
                int colIndex = 1;

                if (dataList[i].FirstOrDefault(x => x.Key == "ManufacturerName").Key == "ManufacturerName" && previousManufacturer != dataList[i].FirstOrDefault(x => x.Key == "ManufacturerName").Value)
                {
                    previousManufacturer = dataList[i].FirstOrDefault(x => x.Key == "ManufacturerName").Value;
                    formulaStartRowIndex = rowIndex;
                    sheet.Cells[pivotRowIndex + 1, pivotColIndex].Value = previousManufacturer;
                    pivotRowIndex++;
                }
                sheet.Cells[pivotRowIndex, pivotColIndex + 1].Formula = $"=SUMPRODUCT(--(FREQUENCY(MATCH({sheetList[2]}!B{formulaStartRowIndex}:B{rowIndex}, {sheetList[2]}!B{formulaStartRowIndex}:B{rowIndex}, 0), ROW({sheetList[2]}!B{formulaStartRowIndex}:B{rowIndex}) - ROW({sheetList[2]}!B{formulaStartRowIndex}) + 1) > 0))";
                colIndex++;
            }
            sheet.Cells[pivotRowIndex + 1, pivotColIndex].Value = "Total";
            sheet.Cells[pivotRowIndex + 1, pivotColIndex + 1].Formula = $"=SUM(B2:B{pivotRowIndex})";

            sheet.Cells[pivotRowIndex + 1, pivotColIndex].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            sheet.Cells[pivotRowIndex + 1, pivotColIndex].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(149, 179, 215));

            sheet.Cells[pivotRowIndex + 1, pivotColIndex + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            sheet.Cells[pivotRowIndex + 1, pivotColIndex + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(149, 179, 215));
        }

        public void AddPivot_Table(List<string> sheetList, ExcelWorksheet sheet, List<Dictionary<string, string>> dataList, ExcelPackage excelPackage)
        {
            //define the data range on the source sheet
            var worksheetData = excelPackage.Workbook.Worksheets[sheetList[2]];
            var dataRange = worksheetData.Cells[worksheetData.Dimension.Address];

            //create the pivot table
            var pivotTable = sheet.PivotTables.Add(sheet.Cells["A2"], dataRange, "PivotTable");

            //label field
            pivotTable.RowFields.Add(pivotTable.Fields["ManufacturerName"]);
            pivotTable.DataOnRows = false;

            //data fields
            var field = pivotTable.DataFields.Add(pivotTable.Fields["TherapAreaName"]);
            field.Function = DataFieldFunctions.Count;
        }

        /// <summary>
        /// Property class for excel sheet
        /// </summary>
        public class ExcelReportFileProperties
        {
            public string HeaderName { get; set; }

            public double Width { get; set; }

            public double Height { get; set; }

            public int RowIndex { get; set; }

            public int ColumnIndex { get; set; }

        }

        #region User Detail Excel Report

        public string GenerateExcelForUser(string url)
        {
            try
            {
                List<List<List<Dictionary<string, string>>>> response = new List<List<List<Dictionary<string, string>>>>();

                List<List<Dictionary<string, string>>> apiResponse = MakeGetRequest(url);

                if (apiResponse.Count() > 0)
                    response.Add(apiResponse);

                List<List<List<Dictionary<string, string>>>> userDetail = new List<List<List<Dictionary<string, string>>>>();

                for (int i = 0; i < 1; i++)
                {
                    var data = response[i];
                    foreach (var valueData in data[0])
                    {
                        string value = string.Empty;
                        valueData.TryGetValue("CustomerOrderId", out value);
                        string apiUrl = $"{ConfigurationManager.AppSettings["BaseAPIURL"]}User/GetUserOrderDetailById?customerOrderId={value}";
                        List<List<Dictionary<string, string>>> userDeatilResponse = MakeGetRequest(apiUrl);
                        if (userDeatilResponse.Count() > 0)
                            userDetail.Add(userDeatilResponse);
                    }
                }
                response.AddRange(userDetail);

                return WriteExcel(response);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return string.Empty;
        }

        private string WriteExcel(List<List<List<Dictionary<string, string>>>> response)
        {
            int sheetIndex = 0;
            string fileSaveInFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data", DateTime.Now.ToFileTime().ToString());
            foreach (var excelSheets in response)
            {
                using (var excelPackage = new ExcelPackage())
                {
                    if (sheetIndex <= 0)
                    {
                        ExcelWorksheet userSheet = excelPackage.Workbook.Worksheets.Add("Abc");
                        userSheet.OutLineApplyStyle = true;

                        foreach (var sheetData in excelSheets)
                        {
                            AddExcelHeader(sheetData, userSheet);
                            AddDataToSheet(userSheet, sheetData);
                        }

                        SaveExcelFile(excelPackage, fileSaveInFolderPath);
                        sheetIndex++;
                    }
                    else
                    {

                        string value = string.Empty;
                        int loopIndex = 0;
                        //ExcelWorksheet userSheet = null;
                        //for (int i = 0; i < excelSheets.Count(); i++)
                        //{
                        //    if (i == 0)
                        //    {
                        //        excelSheets[i][i].TryGetValue("Tabname", out value);
                        //        userSheet = excelPackage.Workbook.Worksheets.Add(value);
                        //        userSheet.OutLineApplyStyle = true;
                        //        AddExcelHeader(excelSheets[i + 1], userSheet);
                        //        AddDataToSheet(userSheet, excelSheets[i + 1]);
                        //    }
                        //    else
                        //    {
                        //        excelSheets[0][1].TryGetValue("Tabname", out value);
                        //        userSheet = excelPackage.Workbook.Worksheets.Add(value);
                        //        userSheet.OutLineApplyStyle = true;
                        //        AddExcelHeader(excelSheets[i + 1], userSheet);
                        //        AddDataToSheet(userSheet, excelSheets[i + 1]);
                        //    }
                        //}

                        ExcelWorksheet userSheet = null;
                        foreach (var sheetData in excelSheets)
                        {
                            if (loopIndex <= 0)
                            {
                                sheetData[0].TryGetValue("Tabname", out value);

                                userSheet = excelPackage.Workbook.Worksheets.Add(value);
                                userSheet.OutLineApplyStyle = true;
                                AddExcelHeader(excelSheets[loopIndex + 1], userSheet);
                                loopIndex++;
                                continue;
                            }

                            bool isKeyNameExist = sheetData.ElementAt(0).ContainsKey("CustomerInvoiceId");
                            if (isKeyNameExist)
                            {
                                excelSheets[0][1].TryGetValue("Tabname", out value);
                                userSheet = excelPackage.Workbook.Worksheets.Add(value);
                                AddExcelHeader(sheetData, userSheet);
                            }

                            AddDataToSheet(userSheet, sheetData);
                            loopIndex++;
                        }
                        SaveExcelFile(excelPackage, fileSaveInFolderPath);
                    }
                }
            }
            return fileSaveInFolderPath;
        }

        public List<List<Dictionary<string, string>>> MakeGetRequest(string url)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var responseData = client.GetAsync(url).Result;
                    if (responseData.StatusCode == HttpStatusCode.OK)
                    {
                        var responseJson = responseData.Content.ReadAsStringAsync().Result;
                        var data = JsonConvert.DeserializeObject<List<List<Dictionary<string, string>>>>(responseJson);
                        return data;
                    }
                }
                return new List<List<Dictionary<string, string>>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void AddExcelHeader(List<Dictionary<string, string>> excelSheets, ExcelWorksheet sheet)
        {
            var excelHeader = new List<ExcelReportFileProperties>();
            var colIndex = 1;
            foreach (KeyValuePair<string, string> headerData in excelSheets[0])
            {
                excelHeader.Add(new ExcelReportFileProperties { HeaderName = headerData.Key, ColumnIndex = colIndex, RowIndex = 1, Width = 40, Height = 30 });
                colIndex++;
            }
            AddHeader(sheet, excelHeader);
        }

        string SaveExcelFile(ExcelPackage excelPackage, string fileSaveInFolderPath)
        {
            try
            {
                // var tempFilePath = Path.GetTempPath(); 
                var tempFilePath = fileSaveInFolderPath;
                var currentPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data");
                if (!Directory.Exists(tempFilePath))
                    Directory.CreateDirectory(tempFilePath);

                var filePath = Path.Combine(tempFilePath, DateTime.Now.ToFileTime().ToString() + ".xlsx");

                excelPackage.SaveAs(new FileInfo(filePath));
                return filePath;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }


        }
        #endregion

        #region Custom file generator

        public string MakeAjaxGetRequest(string url)
        {
            string result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    // client.Timeout = new TimeSpan(0, 10, 0);
                    // client.MaxResponseContentBufferSize = 256000;
                    var responseData = client.GetAsync(url).Result;
                    if (responseData.StatusCode == HttpStatusCode.OK)
                    {
                        result = responseData.Content.ReadAsStringAsync().Result;

                    }
                }
                return result;
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        public async Task<string> MakeAjaxGetRequestAsync(string url)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
            //request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            request.Method = "GET";
            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;

            Task<WebResponse> task = Task.Factory.FromAsync<WebResponse>(request.BeginGetResponse, request.EndGetResponse, null);
            WebResponse response = await task;

            var encoding = ASCIIEncoding.ASCII;
            string responseText = "";
            using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encoding))
            {
                responseText = reader.ReadToEnd();
            }
            return responseText;           
        }

        public string GenerateExcelFile(string url)
        {
            var result = MakeAjaxGetRequest(url);
            var objProcessDetail = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(result);

            string clientId = string.Empty;
            objProcessDetail[0].TryGetValue("ClientID", out clientId);

            string categoryId = string.Empty;
            objProcessDetail[0].TryGetValue("CategoryID", out categoryId);

            string indication = string.Empty;
            objProcessDetail[0].TryGetValue("Indication", out indication);

            string scheduleTypeID = string.Empty;
            objProcessDetail[0].TryGetValue("ScheduleTypeID", out scheduleTypeID);

            string exportFilePath = string.Empty;
            objProcessDetail[0].TryGetValue("ExportFilePath", out exportFilePath);

            var obj = new
            {
                clientId = clientId,
                categoryId = categoryId,
                indicationArray = indication,
                drugArray = string.Empty,
                scheduleTypeID = scheduleTypeID,
            };

            var apiUrl = $"http://localhost:55538/api/CustomFileGenerator/GetCustomFileData?clientId={clientId}&categoryId={categoryId}&indication={indication}&drugArray={string.Empty}&scheduleTypeID={scheduleTypeID}";
            var resultExcelFileData = MakeAjaxGetRequest(apiUrl);

            var excelDataList = JsonConvert.DeserializeObject<List<List<Dictionary<string, string>>>>(resultExcelFileData);

            return WriteExcelData(excelDataList, exportFilePath);
        }

        public string WriteExcelData(List<List<Dictionary<string, string>>> response, string exportFilePath)
        {
            int sheetIndex = 0;
            string fileSaveInFolderPath = $"{exportFilePath}/{ DateTime.Now.ToFileTime().ToString()}";

            using (var excelPackage = new ExcelPackage())
            {
                foreach (var excelSheets in response)
                {
                    if (sheetIndex == 0)
                    {
                        sheetIndex++;
                        continue;
                    }

                    string value = string.Empty;
                    response[0][sheetIndex - 1].TryGetValue("TABNAME", out value);

                    ExcelWorksheet userSheet = excelPackage.Workbook.Worksheets.Add(value);
                    userSheet.OutLineApplyStyle = true;

                    AddExcelHeader(excelSheets, userSheet);

                    AddDataToSheet(userSheet, excelSheets);
                    sheetIndex++;
                }


                SaveExcelFile(excelPackage, fileSaveInFolderPath);
            }
            return fileSaveInFolderPath;
        }

        public void AddExcelFileHeader(Dictionary<string, string> rowData)
        {

        }

        #endregion

        public string GenerateAPiExcelFile(string url, int processId)
        {
            var result = MakeAjaxGetRequest(url);
            var objProcessDetail = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(result);

            string clientId = string.Empty;
            objProcessDetail[0].TryGetValue("ClientID", out clientId);

            string categoryId = string.Empty;
            objProcessDetail[0].TryGetValue("CategoryID", out categoryId);

            string indication = string.Empty;
            objProcessDetail[0].TryGetValue("Indication", out indication);

            string scheduleTypeID = string.Empty;
            objProcessDetail[0].TryGetValue("ScheduleTypeID", out scheduleTypeID);

            string exportFilePath = string.Empty;
            objProcessDetail[0].TryGetValue("ExportFilePath", out exportFilePath);

            var obj = new
            {
                clientId = clientId,
                categoryId = categoryId,
                indicationArray = indication,
                drugArray = string.Empty,
                scheduleTypeID = scheduleTypeID,
            };

            var apiUrl = $"{baseAPIURL}/CustomFileGenerator/GetCustomFileData?clientId={clientId}&categoryId={categoryId}&indication={indication}&drugArray={string.Empty}&scheduleTypeID={scheduleTypeID}";
            var resultExcelFileData = MakeAjaxGetRequest(apiUrl);

            var excelDataList = JsonConvert.DeserializeObject<List<List<Dictionary<string, string>>>>(resultExcelFileData);

            ExcelFileGenerator excelGenerator = new ExcelFileGenerator();
            return excelGenerator.GenerateExcel(processId, excelDataList, exportFilePath);
        }

        public async Task<string> GenerateCustomExcelFileExcelFile(string url)
        {
            var response = await MakeAjaxGetRequestAsync(url);
            //var result = JsonConvert.DeserializeObject<ExcelExportInfo>(response);
           // var result = JsonConvert.DeserializeObject<string>(response);
            return response;
            //DownloadZipFile(result);
        }

        public void DownloadZipFile(string downloadFilePath,string clientName)
        {
            string[] dowloadFileName = downloadFilePath.Split(new[] { "Temp/" }, StringSplitOptions.None);            
            string[] filename = Directory.GetFiles(downloadFilePath);
            string filePath = $"{downloadFilePath}/{dowloadFileName[1]}.zip";
            using (ZipFile zip = new ZipFile())
            {
                string folderName = string.Format("{0} - {1:MMddyyyy - hhmmtt}",
                    dowloadFileName[1],
                    DateTime.Now
                    );
                
                zip.AddFiles(filename, folderName);

                zip.Save(filePath);
            }

            System.Web.HttpContext.Current.Response.ContentType = "application/zip";
            System.Web.HttpContext.Current.Response.AppendHeader("Content-Disposition", $"attachment; filename={dowloadFileName[1]}.zip");
            System.Web.HttpContext.Current.Response.TransmitFile(filePath);
            System.Web.HttpContext.Current.Response.End();

            if (Directory.Exists(downloadFilePath))
                Directory.Delete(downloadFilePath, true);

        }
    }
}