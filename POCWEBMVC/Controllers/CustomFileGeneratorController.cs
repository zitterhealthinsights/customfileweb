﻿using Ionic.Zip;
using POCWEBMVC.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace POCWEBMVC.Controllers
{
    public class CustomFileGeneratorController : Controller
    {
        private static string baseAPIURL = ConfigurationManager.AppSettings["BaseAPIURL"];

        // GET: CustomFileGenerator
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetClientList()
        {
            ExcelHelper excelHelper = new ExcelHelper();
            var result = excelHelper.MakeAjaxGetRequest($"{baseAPIURL}CustomFileGenerator/GetClientList");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetScheduleList()
        {
            ExcelHelper excelHelper = new ExcelHelper();
            var result = excelHelper.MakeAjaxGetRequest($"{baseAPIURL}CustomFileGenerator/GetScheduleList");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProcessList(string clientName, string scheduleType)
        {
            if (clientName.Equals("0"))
                clientName = "<ALL>";

            if (scheduleType.Equals("0"))
                scheduleType = "<ALL>";

            ExcelHelper excelHelper = new ExcelHelper();
            var result = excelHelper.MakeAjaxGetRequest($"{baseAPIURL}CustomFileGenerator/GetProcessList?clientName={clientName}&scheduleType={scheduleType}");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public FileContentResult ExportExcelFile(int processId)
        {

            ExcelHelper excelHelper = new ExcelHelper();
            var downloadPath = excelHelper.GenerateExcelFile($"{baseAPIURL}CustomFileGenerator/GetProcessStepByProcessId?processId={processId}");
            DownloadZipFile(downloadPath);
            return null;
        }

        public void DownloadZipFile(string pathname)
        {

            string[] filename = Directory.GetFiles(pathname);
            string filePath = $"{pathname}/CustomExcelFile.zip";
            using (ZipFile zip = new ZipFile())
            {
                zip.AddFiles(filename, "file");

                zip.Save(filePath);
            }

            Response.ContentType = "application/zip";
            Response.AppendHeader("Content-Disposition", "attachment; filename=CustomExcelFile.zip");
            Response.TransmitFile(filePath);
            Response.End();

            if (Directory.Exists(pathname))
                Directory.Delete(pathname, true);

        }

        public FileContentResult ExportExcelByProcessId(int processId)
        {
            ExcelHelper excelHelper = new ExcelHelper();
            var downloadPath = excelHelper.GenerateAPiExcelFile($"{baseAPIURL}CustomFileGenerator/GetProcessStepByProcessId?processId={processId}", processId);
            DownloadZipFile(downloadPath);
            return null;
        }

        //public async Task<FileContentResult> ExportCustomExcelFile(int processId)
        //{

        //    ExcelHelper excelHelper = new ExcelHelper();
        //    var downloadPath = await excelHelper.GenerateCustomExcelFileExcelFile($"{baseAPIURL}CustomFileGenerator/GenerateCustomExcelFile?processId={processId}");
        //    DownloadZipFile(downloadPath);
        //    return null;
        //}

        [HttpPost]
        public async Task<JsonResult> ExportCustomExcel(int processId)
        {
            ExcelHelper excelHelper = new ExcelHelper();
           var result= await excelHelper.GenerateCustomExcelFileExcelFile($"{baseAPIURL}CustomFileGenerator/GenerateCustomExcelFile?processId={processId}");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult Download(string file,string clientName)
        {
            ExcelHelper excelHelper = new ExcelHelper();
            excelHelper.DownloadZipFile(file,clientName);
            return null;
        }
    }
}