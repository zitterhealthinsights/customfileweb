﻿using POCWEBMVC.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web.Mvc;

namespace POCWEBMVC.Controllers
{
    public class DrugController : Controller
    {
        private static string baseAPIURL = ConfigurationManager.AppSettings["BaseAPIURL"];
        // GET: Drug
        public ActionResult Index()
        {
            return View();
        }

        public FileContentResult ExportSingleSheetExcel()
        {
            ExcelHelper excelHelper = new ExcelHelper();
            List<string> url = new List<string>() { baseAPIURL + "Drug/GetAllDrug" };
            List<string> excelSheet = new List<string>() { "Drug Report" };
            List<List<string>> header = new List<List<string>>()
            {
                new List<string>(){ "Drug ID", "Drug Name" },
                new List<string>(){ "Manufacturer ID", "Manufacturer Name", "LastUpdated", "LastUpdatedBy", "Active" }
            };

            var path = excelHelper.GenerateExcelFromJSON(url, excelSheet, header);

            DownloadExcelFile(path, "Drug");

            return null;
        }

        public FileContentResult ExportMultiSheetExcel()
        {
            ExcelHelper excelHelper = new ExcelHelper();

            List<string> url = new List<string>() { baseAPIURL + "Drug/GetAllDrug", baseAPIURL + "Manufacturer/GetAllManufacturer" };
            List<string> excelSheet = new List<string>() { "Drug Report", "Manufacturer Report" };
            List<List<string>> header = new List<List<string>>()
            {
                new List<string>(){ "DrugID", "DrugName" },
                new List<string>(){ "ManufacturerID", "ManufacturerName"}
            };
            var path = excelHelper.GenerateExcelFromJSON(url, excelSheet, header);

            DownloadExcelFile(path, "Drug_Manufacturer");

            return null;
        }

        public FileContentResult ExportDrugManufactureTAExcel()
        {
            ExcelHelper excelHelper = new ExcelHelper();

            List<string> url = new List<string>() { baseAPIURL + "Drug/GetAllDrug", baseAPIURL + "Manufacturer/GetAllManufacturer", baseAPIURL + "TherapArea/GetAll" };
            List<string> excelSheet = new List<string>() { "Drug Report", "Manufacturer Report", "TherapAreaName" };
            List<List<string>> header = new List<List<string>>()
            {
                new List<string>(){ "DrugID", "DrugName"},
                new List<string>(){ "ManufacturerID", "ManufacturerName" },
                new List<string>(){ "TherapAreaID", "TherapAreaName" }
            };
            var path = excelHelper.GenerateExcelFromJSON(url, excelSheet, header);
            DownloadExcelFile(path, "Drug_Manufacturer_TherapArea");
            return null;
        }

        public FileContentResult ExportDrugManufactureTAExcelWithPivot()
        {
            ExcelHelper excelHelper = new ExcelHelper();

            List<string> url = new List<string>() { baseAPIURL + "Drug/GetAllDrug", baseAPIURL + "Manufacturer/GetAllManufacturer", baseAPIURL + "Manufacturer/GetAllManufacturerWithPivot", baseAPIURL + "Manufacturer/GetAllManufacturerWithPivot" };
            List<string> excelSheet = new List<string>() { "Drug Report", "Manufacturer Report", "ManufacturerWithTherapArea", "PivotTable" };
            List<List<string>> header = new List<List<string>>()
            {
                new List<string>(){ "DrugID", "DrugName"},
                new List<string>(){ "ManufacturerID", "ManufacturerName" },
                new List<string>(){ "ManufacturerName", "TherapAreaName"},
                new List<string>(){ "", ""}
            };
            var path = excelHelper.GenerateExcelFromJSON(url, excelSheet, header);

            DownloadExcelFile(path, "Drug_Manufacturer_Pivot");

            return null;
        }


        /// <summary>
        /// Method to download excel file
        /// </summary>
        /// <param name="path"></param>
        public void DownloadExcelFile(string path, string fileName)
        {

            System.IO.FileInfo file = new System.IO.FileInfo(path);
            if (file.Exists)
            {
                Response.Clear();
                //Content-Disposition will tell the browser how to treat the file.(e.g. in case of jpg file, Either to display the file in browser or download it)
                //Here the attachement is important. which is telling the browser to output as an attachment and the name that is to be displayed on the download dialog
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ".xlsx");
                //Telling length of the content..
                Response.AddHeader("Content-Length", file.Length.ToString());

                //Type of the file, whether it is exe, pdf, jpeg etc etc
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                //Writing the content of the file in response to send back to client..
                Response.WriteFile(file.FullName);
                Response.End();

                // Delete the file... 
                System.IO.File.Delete(file.FullName);

            }
            else
            {
                Response.Write("This file does not exist.");
            }
        }
    }
}