﻿using Ionic.Zip;
using POCWEBMVC.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POCWEBMVC.Controllers
{
    public class UserController : Controller
    {
        private static string baseAPIURL = ConfigurationManager.AppSettings["BaseAPIURL"];

        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public FileContentResult ExportUserExcelSheet()
        {
            ExcelHelper excelHelper = new ExcelHelper();
            string url = $"{baseAPIURL}/User/GetUserDetailById?userId=17";
            string downloadPath = excelHelper.GenerateExcelForUser(url);
            DownloadZipFile(downloadPath);
            return null;
        }

        public void DownloadExcelFile(string path, string fileName)
        {

            System.IO.FileInfo file = new System.IO.FileInfo(path);
            if (file.Exists)
            {
                Response.Clear();
                //Content-Disposition will tell the browser how to treat the file.(e.g. in case of jpg file, Either to display the file in browser or download it)
                //Here the attachement is important. which is telling the browser to output as an attachment and the name that is to be displayed on the download dialog
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ".xlsx");
                //Telling length of the content..
                Response.AddHeader("Content-Length", file.Length.ToString());

                //Type of the file, whether it is exe, pdf, jpeg etc etc
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                //Writing the content of the file in response to send back to client..
                Response.WriteFile(file.FullName);
                Response.End();

                // Delete the file... 
                System.IO.File.Delete(file.FullName);

            }
            else
            {
                Response.Write("This file does not exist.");
            }
        }

        public void DownloadZipFile(string pathname)
        {

            string[] filename = Directory.GetFiles(pathname);
            string filePath = $"{pathname}/zipa.zip";
            using (ZipFile zip = new ZipFile())
            {
                zip.AddFiles(filename, "file");

                zip.Save(filePath);
            }

            Response.ContentType = "application/zip";
            Response.AppendHeader("Content-Disposition", "attachment; filename=3 tier Sample.zip");
            Response.TransmitFile(filePath);
            Response.End();

            if (Directory.Exists(pathname))
                Directory.Delete(pathname, true);

        }
    }
}