﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using static POCWEBMVC.Common.ExcelHelper;

namespace POCWEBMVC.ExcelExtension.ClientExcelFiles
{
    public class McCannHCHLStateFile_RegeneronSanofi : IExcelFileInfo
    {
        private int processId = 19;

        public int ProcessId
        {
            get
            {
                return this.processId;
            }
        }

        public string ProcessTypeName
        {
            get
            {
                return GetProcessNameByProcessId();
            }
        }

        public string DownloadZipFile(int processId, List<List<Dictionary<string, string>>> response, string exportFilePath)
        {
            int sheetIndex = 0;
            string fileSaveInFolderPath = $"{exportFilePath}/{ DateTime.Now.ToFileTime().ToString()}";

            using (var excelPackage = new ExcelPackage())
            {
                foreach (var excelSheets in response)
                {
                    if (sheetIndex == 0)
                    {
                        sheetIndex++;
                        continue;
                    }

                    string value = string.Empty;
                    response[0][sheetIndex - 1].TryGetValue("TABNAME", out value);

                    ExcelWorksheet userSheet = excelPackage.Workbook.Worksheets.Add(value);
                    userSheet.OutLineApplyStyle = true;

                    AddExcelHeader(excelSheets, userSheet);

                    AddDataToSheet(userSheet, excelSheets);
                    sheetIndex++;
                }


                SaveExcelFile(excelPackage, fileSaveInFolderPath);
            }
            return fileSaveInFolderPath;
        }

        void AddExcelHeader(List<Dictionary<string, string>> excelSheets, ExcelWorksheet sheet)
        {
            var excelHeader = new List<ExcelReportFileProperties>();
            var colIndex = 1;
            foreach (KeyValuePair<string, string> headerData in excelSheets[0])
            {
                excelHeader.Add(new ExcelReportFileProperties { HeaderName = headerData.Key, ColumnIndex = colIndex, RowIndex = 1, Width = 40, Height = 30 });
                colIndex++;
            }
            AddHeader(sheet, excelHeader);
        }

        void AddHeader(ExcelWorksheet sheet, IList<ExcelReportFileProperties> headers)
        {
            foreach (var header in headers)
            {

                sheet.Cells[header.RowIndex, header.ColumnIndex].Value = header.HeaderName;
                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.Font.Bold = true;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.WrapText = true;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                sheet.Cells[header.RowIndex, header.ColumnIndex].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                sheet.Column(header.ColumnIndex).Width = header.Width;

                sheet.Row(header.RowIndex).Height = header.Height;

            }

        }

        string SaveExcelFile(ExcelPackage excelPackage, string fileSaveInFolderPath)
        {
            try
            {
                // var tempFilePath = Path.GetTempPath(); 
                var tempFilePath = fileSaveInFolderPath;
                if (!Directory.Exists(tempFilePath))
                    Directory.CreateDirectory(tempFilePath);

                var filePath = Path.Combine(tempFilePath, ProcessTypeName + ".xlsx");

                excelPackage.SaveAs(new FileInfo(filePath));
                return filePath;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }


        }

        void AddDataToSheet(ExcelWorksheet sheet, List<Dictionary<string, string>> dataList)
        {
            for (int i = 0; i < dataList.Count(); i++)
            {
                var rowIndex = i + 2;
                var data = dataList[i];
                int colIndex = 1;
                foreach (var valueData in data)
                {
                    sheet.Cells[rowIndex, colIndex].Value = valueData.Value;
                    colIndex++;
                }

            }
        }

        string GetProcessNameByProcessId()
        {
            string processTypeName = string.Empty;

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["CustomFileGeneratorDbContext"].ConnectionString))
            {
                connection.Open();
                string strQuery = "select * from process where processId = @processId";
                SqlCommand cmd = new SqlCommand(strQuery, connection);
                cmd.Parameters.AddWithValue("@processId", processId);

                // get query results

                SqlDataReader rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    processTypeName = rdr["ProcessName"].ToString();
                }

                return processTypeName;
            }
        }
    }
}