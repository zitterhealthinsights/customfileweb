﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POCWEBMVC.ExcelExtension
{
    public abstract class ExcelFileAbstract : IExcelFileInfo
    {
        public virtual int ProcessId { get; }

        public abstract string DownloadZipFile(int processId, List<List<Dictionary<string, string>>> response, string exportFilePath);

        public void SaveExcelFile()
        {

        }

    }
}