﻿using POCWEBMVC.ExcelExtension.ClientExcelFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POCWEBMVC.ExcelExtension
{
    public class ExcelFileGenerator
    {
        List<IExcelFileInfo> _excelFileGenerator = new List<IExcelFileInfo>();
        public ExcelFileGenerator()
        {
            _excelFileGenerator.Add(new DefaultExcelFile());
            _excelFileGenerator.Add(new RegeneronSanofiCustomFileHCHLRAFiles_RegeneronSanofi());
            _excelFileGenerator.Add(new PayerSciencesHCHLFile_RegeneronSanofi());
            _excelFileGenerator.Add(new McCannHCHLStateFile_RegeneronSanofi());
            _excelFileGenerator.Add(new ProteanHCHLStateFile_RegeneronSanofi());
        }

        public string GenerateExcel(int processId, List<List<Dictionary<string, string>>> response, string exportFilePath)
        {
            var objExcelFile = _excelFileGenerator.Find(x => x.ProcessId == processId);
            if (objExcelFile == null)
            {
                var excelGenerator = _excelFileGenerator.Find(x => x.ProcessId == 0);                
                return excelGenerator.DownloadZipFile(processId, response, exportFilePath);
            }
            return objExcelFile.DownloadZipFile(processId, response, exportFilePath);
        }
    }
}