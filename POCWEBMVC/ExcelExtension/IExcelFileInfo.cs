﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POCWEBMVC.ExcelExtension
{
    public interface IExcelFileInfo
    {
        int ProcessId { get; }

        string ProcessTypeName { get; }

        string DownloadZipFile(int processId, List<List<Dictionary<string, string>>> response, string exportFilePath);

    }
}