﻿var customFileGenerator;
var isDownloadedFile = false;
var baseUrl = window.location.href;
(function () {

    customFileGenerator = {

        GetClientList: function () {            
            $.ajax({
                url: baseUrl + '/CustomFileGenerator/GetClientList',
                type: 'GET',
                dataType: 'json',
                success: function (data) {                    
                    var result = JSON.parse(data);
                    if (result != null && result.length) {
                        $.each(result, function (index, item) {
                            $('#ddlClient')
                                .append($("<option></option>")
                                    .attr("value", index == 0 ? "0" : item.Name)
                                    .text(item.Name));
                        });
                    }
                },
                error: function () {
                }
            });
        },

        GetScheduleList: function () {
            $.ajax({
                url: baseUrl + '/CustomFileGenerator/GetScheduleList',
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    var result = JSON.parse(data);
                    if (result != null && result.length) {
                        $.each(result, function (index, item) {
                            $('#ddlSchedule')
                                .append($("<option></option>")
                                    .attr("value", index == 0 ? "0" : item.ScheduleTypeDescription)
                                    .text(item.ScheduleTypeDescription));
                        });
                    }
                },
                error: function () {
                }
            });
        },

        GetProcessList: function () {
            var ddlClientVal = $('#ddlClient').find(":selected").val();
            var ddlScheduleVal = $('#ddlSchedule').find(":selected").val();

            $.ajax({
                url: baseUrl + "/CustomFileGenerator/GetProcessList?clientName=" + ddlClientVal + "&scheduleType=" + ddlScheduleVal,
                type: 'GET',
                dataType: 'json',
                success: function (data) {                    
                    var result = JSON.parse(data);
                    $('#noRecord').hide();

                    if ($('table tbody tr').length > 0)
                        $('table tbody tr').not(':first').remove();

                    if (result.length > 0) {

                        $.each(result, function (index, item) {
                            var tableRows = "<tr><td>" + item.ProcessName + "</td><td>" + item.ClientName + "</td><td>" + item.ScheduleTypeDescription + "</td><td><button type='button' class='btn btn- success' onclick='customFileGenerator.ExportExcelFile(" + item.ProcessID + ");'>Export File</button> </td></tr>";
                            $('table tbody').append(tableRows);
                        });
                        return;
                    }

                    $('#noRecord').show();
                },
                error: function () {
                }
            });
        },

        ExportExcelFile: function (arg) {
            isDownloadedFile = false;            

            //$.blockUI({ message: $('#loader').html(), css: { width: '200px', border: 'none', backgroundColor: 'transparent',left:'38%' } });
            $('#loader-wrapper').show();
            
            var selectedProcessList = [];
            if ($('table tbody tr').length > 0) {
                $.each($('table tbody tr'), function (index, item) {                    
                    selectedProcessList.push($(item).find('span').text());
                });
            }
            var postObj = {
                processList: selectedProcessList
            };

            //setTimeout(function () {
            //    if (!isDownloadedFile) {
            //        //$.blockUI({ message: "<h1>The Excel file takes time more than expected. It will be downloaded automatically......</h1>", css: { width: '70%', left: '15%' } });                    
            //        $('#loader').hide();
            //        $('#loader-text').show();
            //        setTimeout(function () {
            //            $('#loader-text').hide();
            //            $('#loader').show();
            //            $('#loader-wrapper').hide();
            //            //$.unblockUI(); }
            //        }, 1000);
            //        isDownloadedFile = false;
            //    }
            //}, 15000);

            $.ajax({
                url: baseUrl + "/CustomFileGenerator/ExportCustomExcel?processId=" + arg,
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (data) {                    
                    var result = JSON.parse(data);  
                    //$.unblockUI();
                    $('#loader-wrapper').hide();
                    //isDownloadedFile = true;
                    window.location = '/CustomFileGenerator/Download?file=' + result.FilePath + "&clientName=" + result.ClientName;
                },
                error: function () {
                   // $.unblockUI();
                }
            });

        }

    };

    customFileGenerator.GetClientList();
    customFileGenerator.GetScheduleList();
})();