﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FileMaker.Services;
using FileMaker.Services.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CustomFileGenerator.Controllers
{
    [Produces("application/json")]
    [Route("api/processes")]
    public class ProcessesController : Controller
    {
        // GET: api/processes/5
        [HttpGet("{clientId}/{processId}", Name = "Get")] // TODO: Sandoz gets one more parameter from UI, months count
        public IActionResult Get(int clientId, int processId)
        {
            // Client decides about folder for IProcessFilesMaker
            // Multiple requests can come simultaniously, so folder should be unique 
            string Folder = @"C:\Temp\" + DateTime.Now.ToString("yyyyMMddHHmmssfffffff") + Path.DirectorySeparatorChar;
            Directory.CreateDirectory(Folder);

            ProcessFilesMaker pfm = new ProcessFilesMaker(Folder, processId, clientId);
            try
            {
                string OutputZipLocation = pfm.CreateProcessFiles();
                FileContentResult Result = new FileContentResult(System.IO.File.ReadAllBytes(OutputZipLocation), "application/zip")
                {
                    FileDownloadName = Path.GetFileName(OutputZipLocation)
                };
                return Result;
            }
            catch (InvalidProcessIdException ex)
            {
                return BadRequest(ex.Message);
                //return NotFound(ex.Message);
            }
            catch (ProcessForbiddenForClientException ex) {
                return BadRequest(ex.Message);
                //return Forbid(ex.Message);
            }
            catch (Exception)
            {
                return StatusCode(500);
                //throw ex;
            }
        }
    }
}