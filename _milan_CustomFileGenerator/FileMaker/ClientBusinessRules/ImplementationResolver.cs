﻿using System;
using System.Collections.Generic;
using System.Reflection;
using FileMaker.ClientBusinessRules.Interface;
using FileMaker.ClientBusinessRules.Implementations;
using FileMaker.Services.Exceptions;

namespace FileMaker.ClientBusinessRules
{
    class ImplementationResolver
    {
        private List<ICustomFileSettings> ImplementationObjects;

        public ImplementationResolver()
        {
            Assembly[] AllAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly assembly in AllAssemblies)
            {
                if (assembly.FullName.StartsWith("FileMaker"))
                {
                    ImplementationObjects = new List<ICustomFileSettings>();
                    IEnumerable<TypeInfo> TypeInfos = assembly.DefinedTypes;
                    foreach (TypeInfo typeInfo in TypeInfos)
                    {
                        if (typeInfo.FullName.StartsWith("FileMaker.ClientBusinessRules.Implementations"))
                        {
                            ImplementationObjects.Add((ICustomFileSettings) Activator.CreateInstance(typeInfo.UnderlyingSystemType));
                        }
                    }
                }
            }

            //Implementations = new List<ICustomFileSettings>();
            //Implementations.Add(new Lilly_BcFlat());
            //Implementations.Add(new Lilly_BcPbm());
            //Implementations.Add(new Lilly_BcState());
            //Implementations.Add(new Sandoz_SandozCustomFile());
        }

        public ICustomFileSettings GetCustomFileSettings(int processId, int clientId)
        {
            foreach (ICustomFileSettings implementationObject in ImplementationObjects)
            {
                if (implementationObject.GetProcessDescriptor().ProcessId == processId)
                {
                    if (implementationObject.GetProcessDescriptor().ClientId == clientId)
                    {
                        return implementationObject;
                    }
                    else
                    {
                        throw new ProcessForbiddenForClientException(string.Format("Process with ID {0} is forbidden for client with ID {1}", processId, clientId));
                    }
                }
            }

            throw new InvalidProcessIdException(string.Format("Process with ID {0} is not found.", processId));
        }
    }
}
