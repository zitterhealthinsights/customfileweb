﻿using System.Collections.Generic;
using FileMaker.TaskDescriptors;
using FileMaker.ClientBusinessRules.Interface;

namespace FileMaker.ClientBusinessRules.Implementations
{
    class Lilly_BcFlat : ICustomFileSettings
    {
        public ProcessDescriptor GetProcessDescriptor()
        {
            SheetDescriptor Sheet0 = new SheetDescriptor(
                1,
                0,
                "BC",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { });

            FileDescriptor File0 = new FileDescriptor(
                "ZHIPATTLillyBreastCancerFlatFile_{timestamp}.xlsx",
                NameTimeStampRule.NOW__MMddyyyy,
                0,
                new List<FileCustomization>() { FileCustomization.LILLY_XLSX_TO_CSV },
                new List<SheetDescriptor>() { Sheet0 });

            return new ProcessDescriptor(
                37,
                83,
                "Lilly - BC FLAT",
                new List<ProcessCustomization>() { },
                new List<FileDescriptor>() { File0 });
        }
    }
}
