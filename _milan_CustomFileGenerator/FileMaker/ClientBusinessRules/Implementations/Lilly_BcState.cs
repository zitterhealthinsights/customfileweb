﻿using System.Collections.Generic;
using FileMaker.TaskDescriptors;
using FileMaker.ClientBusinessRules.Interface;

namespace FileMaker.ClientBusinessRules.Implementations
{
    class Lilly_BcState : ICustomFileSettings
    {
        public ProcessDescriptor GetProcessDescriptor()
        {
            SheetDescriptor Sheet0 = new SheetDescriptor(
                3,
                0,
                "BC",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { });

            FileDescriptor File0 = new FileDescriptor(
                "ZHIPATTLillyBreastCancerFlatFilebyState_{timestamp}.xlsx",
                NameTimeStampRule.NOW__MMddyyyy,
                0,
                new List<FileCustomization>() { FileCustomization.LILLY_XLSX_TO_CSV },
                new List<SheetDescriptor>() { Sheet0 });

            return new ProcessDescriptor(
                39,
                83,
                "Lilly - BC STATE",
                new List<ProcessCustomization>() { },
                new List<FileDescriptor>() { File0 });
        }
    }
}