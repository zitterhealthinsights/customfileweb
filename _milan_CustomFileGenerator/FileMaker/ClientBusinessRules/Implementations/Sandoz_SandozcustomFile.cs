﻿using System.Collections.Generic;
using FileMaker.TaskDescriptors;
using FileMaker.ClientBusinessRules.Interface;

namespace FileMaker.ClientBusinessRules.Implementations
{
    class Sandoz_SandozCustomFile : ICustomFileSettings
    {
        public ProcessDescriptor GetProcessDescriptor()
        {

            SheetDescriptor Sheet00 = new SheetDescriptor(
                4,
                0,
                "NEU Recent Changes",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { SheetCustomization.DELETE_SHEET_CONTENT });

            SheetDescriptor Sheet01 = new SheetDescriptor(
                5,
                1,
                "NEU",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet02 = new SheetDescriptor(
                6,
                2,
                "NEU PBM",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet03 = new SheetDescriptor(
                7,
                3,
                "NEU by State",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet04 = new SheetDescriptor(
                8,
                4,
                "NEU_DD",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { SheetCustomization.DATA_DICTIONARY_FORMATTING });

            SheetDescriptor Sheet05 = new SheetDescriptor(
                9,
                5,
                "Definition",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { SheetCustomization.DEFINITION_FORMATTING });

            FileDescriptor File0 = new FileDescriptor(
                "Zitter PA Tracking Tool Sandoz NEU {timestamp}.xlsx",
                NameTimeStampRule.NOW_MINUS_ONE_MONTH__MMMM_SPACE_yyyy,
                0,
                new List<FileCustomization>() { FileCustomization.CREATE_PIVOTS_SANDOZ },
                new List<SheetDescriptor>() { Sheet00, Sheet01, Sheet02, Sheet03, Sheet04, Sheet05 });


            /*



            SheetDescriptor Sheet10 = new SheetDescriptor(
                10,
                0,
                "GHD Recent Changes",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { SheetCustomization.DELETE_SHEET_CONTENT });

            SheetDescriptor Sheet11 = new SheetDescriptor(
                11,
                1,
                "GHD",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet12 = new SheetDescriptor(
                12,
                2,
                "GHD PBM",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet13 = new SheetDescriptor(
                13,
                3,
                "GHD by State",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet14 = new SheetDescriptor(
                14,
                4,
                "GHD_DD",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { SheetCustomization.DATA_DICTIONARY_FORMATTING });

            SheetDescriptor Sheet15 = new SheetDescriptor(
                15,
                5,
                "Definition",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { SheetCustomization.DEFINITION_FORMATTING });

            FileDescriptor File1 = new FileDescriptor(
                "Zitter PA Tracking Tool Sandoz GHD {timestamp}.xlsx",
                NameTimeStampRule.NOW_MINUS_ONE_MONTH__MMMM_SPACE_yyyy,
                1,
                new List<FileCustomization>() { FileCustomization.RECENT_CHANGES_CREATE_PIVOTS, FileCustomization.PIVOTS_SETTINGS_SANDOZ },
                new List<SheetDescriptor>() { Sheet10, Sheet11, Sheet12, Sheet13, Sheet14, Sheet15 });





            SheetDescriptor Sheet20 = new SheetDescriptor(
                16,
                0,
                "CLL Recent Changes",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { SheetCustomization.DELETE_SHEET_CONTENT });

            SheetDescriptor Sheet21 = new SheetDescriptor(
                17,
                1,
                "CLL",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet22 = new SheetDescriptor(
                18,
                2,
                "CLL PBM",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet23 = new SheetDescriptor(
                19,
                3,
                "CLL by State",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet24 = new SheetDescriptor(
                20,
                4,
                "NHL Recent Changes",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { SheetCustomization.DELETE_SHEET_CONTENT });

            SheetDescriptor Sheet25 = new SheetDescriptor(
                21,
                5,
                "NHL",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet26 = new SheetDescriptor(
                22,
                6,
                "NHL PBM",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet27 = new SheetDescriptor(
                23,
                7,
                "NHL by State",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet28 = new SheetDescriptor(
                24,
                8,
                "CLL_DD",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { SheetCustomization.DATA_DICTIONARY_FORMATTING });

            SheetDescriptor Sheet29 = new SheetDescriptor(
                25,
                9,
                "NHL_DD",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { SheetCustomization.DATA_DICTIONARY_FORMATTING });

            SheetDescriptor Sheet210 = new SheetDescriptor(
                26,
                10,
                "Definition",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { SheetCustomization.DEFINITION_FORMATTING });

            FileDescriptor File2 = new FileDescriptor(
                "Zitter PA Tracking Tool Sandoz CLL NHL {timestamp}.xlsx",
                NameTimeStampRule.NOW_MINUS_ONE_MONTH__MMMM_SPACE_yyyy,
                2,
                new List<FileCustomization>() { FileCustomization.RECENT_CHANGES_CREATE_PIVOTS, FileCustomization.PIVOTS_SETTINGS_SANDOZ },
                new List<SheetDescriptor>() { Sheet20, Sheet21, Sheet22, Sheet23, Sheet24, Sheet25, Sheet26, Sheet27, Sheet28, Sheet29, Sheet210 });





            SheetDescriptor Sheet30 = new SheetDescriptor(
                27,
                0,
                "RA Recent Changes",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { SheetCustomization.DELETE_SHEET_CONTENT });

            SheetDescriptor Sheet31 = new SheetDescriptor(
                28,
                1,
                "RA",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet32 = new SheetDescriptor(
                29,
                2,
                "RA PBM",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet33 = new SheetDescriptor(
                30,
                3,
                "RA by State",
                new List<DataCustomization>() { DataCustomization.SORT_BY_MCOID_ASC },
                new List<SheetCustomization>() { SheetCustomization.STANDARD_FORMATTING });

            SheetDescriptor Sheet34 = new SheetDescriptor(
                31,
                4,
                "RA_DD",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { SheetCustomization.DATA_DICTIONARY_FORMATTING });

            SheetDescriptor Sheet35 = new SheetDescriptor(
                32,
                5,
                "Definition",
                new List<DataCustomization>() { },
                new List<SheetCustomization>() { SheetCustomization.DEFINITION_FORMATTING });

            FileDescriptor File3 = new FileDescriptor(
                "Zitter PA Tracking Tool Sandoz RA {timestamp}.xlsx",
                NameTimeStampRule.NOW_MINUS_ONE_MONTH__MMMM_SPACE_yyyy,
                3,
                new List<FileCustomization>() { FileCustomization.RECENT_CHANGES_CREATE_PIVOTS, FileCustomization.PIVOTS_SETTINGS_SANDOZ },
                new List<SheetDescriptor>() { Sheet30, Sheet31, Sheet32, Sheet33, Sheet34, Sheet35 });


    */


            return new ProcessDescriptor(
                16,
                67,
                "Sandoz Custom File",
                new List<ProcessCustomization>() { },
                new List<FileDescriptor>() { File0/*, File1, File2 , File3*/});
        }
    }
}