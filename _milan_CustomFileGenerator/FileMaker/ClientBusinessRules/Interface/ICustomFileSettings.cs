﻿using FileMaker.TaskDescriptors;

namespace FileMaker.ClientBusinessRules.Interface
{
    interface ICustomFileSettings
    {
        ProcessDescriptor GetProcessDescriptor();
    }
}
