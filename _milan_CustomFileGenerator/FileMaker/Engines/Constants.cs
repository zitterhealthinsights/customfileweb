﻿namespace FileMaker.Engines
{
    static class Constants
    {
        public static string EXCEL_FILE_EXTENSION = ".xlsx";
        public static string CSV_FILE_EXTENSION = ".csv";
        public static string ZIP_FILE_EXTENSION = ".zip";
        public static bool MAKE_FILES_PARALLEL = false;
        public static bool MAKE_SHEETS_PARALLEL = false;
    }
}
