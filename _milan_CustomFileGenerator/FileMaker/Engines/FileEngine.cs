﻿using FileMaker.ExcelLibrary;
using FileMaker.TaskDescriptors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace FileMaker.Engines
{
    class FileEngine
    {
        private FileDescriptor FileSettings;
        private string Folder;
        private int ClientId;

        public FileEngine(FileDescriptor fileSettings, string folder, int clientId)
        {
            FileSettings = fileSettings;
            Folder = folder;
            ClientId = clientId;
        }

        public void Make() {

            List<SheetEngine> SheetEngines = new List<SheetEngine>();
            FillSheetEngines(SheetEngines);
            RunSheetEngines(SheetEngines);
            
            string FileLocation = ComposeFileLocation(Folder, FileSettings);
            JoinSheets(Folder, FileLocation);

            ApplyFileCustomizations(FileLocation, FileSettings.FileCustomizations);
        }

        private void FillSheetEngines(List<SheetEngine> sheetEngines) {

            foreach (SheetDescriptor SheetSettings in FileSettings.SheetsDescriptors)
            {
                string SheetFolder = Folder + "SHEET" + SheetSettings.SheetIndex.ToString("D3") + Path.DirectorySeparatorChar;
                Directory.CreateDirectory(SheetFolder);

                SheetEngine SheetEngine = new SheetEngine(SheetSettings, SheetFolder, ClientId);
                sheetEngines.Add(SheetEngine);
            }
        }

        private void RunSheetEngines(List<SheetEngine> sheetEngines) {

            if (Constants.MAKE_SHEETS_PARALLEL)
            {
                Parallel.ForEach(sheetEngines, (currentSheetEngine) =>
                {
                    currentSheetEngine.Make();
                }
                );
            }
            else
            {
                foreach (SheetEngine se in sheetEngines)
                {
                    se.Make();
                }
            }
        }

        private string ComposeFileLocation(string folder, FileDescriptor fileSettings)
        {
            string FileLocation = "";

            string FileName = fileSettings.FileName;
            if (FileName.Contains("{timestamp}"))
            {
                switch (fileSettings.NameTimeStampRule)
                {
                    case NameTimeStampRule.NOW__MMddyyyy:
                        FileName = FileName.Replace("{timestamp}", DateTime.Now.ToString("MMddyyyy"));
                        break;

                    case NameTimeStampRule.NOW_MINUS_ONE_MONTH__MMMM_SPACE_yyyy:
                        FileName = FileName.Replace("{timestamp}", DateTime.Now.AddMonths(-1).ToString("MMMM yyyy"));
                        break;

                    default:
                        break;
                }
            }

            FileLocation = folder + FileName;

            return FileLocation;
        }

        private void JoinSheets(string folder, string resultFileLocation) {

            string[] SheetFilesLocations = Directory.GetFiles(folder, "*", SearchOption.AllDirectories);
            Array.Sort(SheetFilesLocations);

            IExcelLibrary excelLibrary = new ExcelLibraryOfficeOpenXml();
            excelLibrary.CreateMergedFile(resultFileLocation, SheetFilesLocations);

            foreach (string SheetFileLocation in SheetFilesLocations)
            {
                File.Delete(SheetFileLocation);
            }
            string[] SheetFilesFolders = Directory.GetDirectories(folder);
            foreach (string SheetFilesFolder in SheetFilesFolders)
            {
                Directory.Delete(SheetFilesFolder, true);
            }
        }

        private void ApplyFileCustomizations(string excelFileLocation, List<FileCustomization> fileCustomizations) {

            IExcelLibrary excelLibrary = new ExcelLibraryOfficeOpenXml();

            foreach (FileCustomization fc in fileCustomizations)
            {
                switch (fc)
                {
                    case FileCustomization.LILLY_XLSX_TO_CSV:
                        string csvFileLocation = excelFileLocation.Replace(Constants.EXCEL_FILE_EXTENSION, Constants.CSV_FILE_EXTENSION);
                        excelLibrary.SingleSheetFileToPipeDelimitedFile(excelFileLocation, csvFileLocation);
                        File.Delete(excelFileLocation);
                        break;

                    case FileCustomization.CREATE_PIVOTS_SANDOZ:
                        excelLibrary.CreatePivotsSandoz(excelFileLocation);
                        break;

                    default:
                        break;
                }
            }
        }

    }
}
