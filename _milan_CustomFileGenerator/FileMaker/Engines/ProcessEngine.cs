﻿using System.Collections.Generic;
using FileMaker.ClientBusinessRules;
using System.IO;
using FileMaker.TaskDescriptors;
using System.Threading.Tasks;
using System;
using System.IO.Compression;
using FileMaker.ClientBusinessRules.Interface;


namespace FileMaker.Engines
{
    class ProcessEngine
    {
        int ProcessId;
        string Folder;
        private int ClientId;

        public ProcessEngine(int processId, string folder, int clientId)
        {
            ProcessId = processId;
            Folder = folder;
            ClientId = clientId;
        }

        public string Make() {

            string OutputZipLocation = "";

            ImplementationResolver Resolver = new ImplementationResolver();
            ICustomFileSettings ProcessSettings = Resolver.GetCustomFileSettings(ProcessId, ClientId);
            ProcessDescriptor ProcessDescriptor = ProcessSettings.GetProcessDescriptor();

            List<FileEngine> FileEngines = new List<FileEngine>();
            FillFileEngines(FileEngines, ProcessDescriptor.FilesDescriptors);
            RunFileEngines(FileEngines);

            // TODO: ApplyProcessCustomizations

            OutputZipLocation = Folder + ProcessDescriptor.ProcessName + Constants.ZIP_FILE_EXTENSION;
            ZipProducedFiles(Folder, OutputZipLocation);

            return OutputZipLocation;
        }

        private void FillFileEngines(List<FileEngine> fileEngines, List<FileDescriptor> filesDescriptors)
        {
            foreach (FileDescriptor FileDescriptor in filesDescriptors)
            {
                string FileFolder = Folder + "FILE" + FileDescriptor.FileIndex.ToString("D3") + Path.DirectorySeparatorChar;
                Directory.CreateDirectory(FileFolder);

                FileEngine FileEngine = new FileEngine(FileDescriptor, FileFolder, ClientId);
                fileEngines.Add(FileEngine);
            }
        }

        private void RunFileEngines(List<FileEngine> fileEngines)
        {

            if (Constants.MAKE_FILES_PARALLEL)
            {
                Parallel.ForEach(fileEngines, (currentFileEngine) =>
                {
                    currentFileEngine.Make();
                }
                );
            }
            else
            {
                foreach (FileEngine fe in fileEngines)
                {
                    fe.Make();
                }
            }
        }

        private void ZipProducedFiles(string producedFilesRootFolder, string zipFileLocation) {

            string[] ProducedFilesLocations = Directory.GetFiles(producedFilesRootFolder, "*", SearchOption.AllDirectories);
            Array.Sort(ProducedFilesLocations);

            ZipArchive zip = ZipFile.Open(zipFileLocation, ZipArchiveMode.Create);
            foreach (string file in ProducedFilesLocations)
            {
                zip.CreateEntryFromFile(file, Path.GetFileName(file), CompressionLevel.Optimal);
            }
            zip.Dispose();

            foreach (string file in ProducedFilesLocations)
            {
                File.Delete(file);
            }
            string[] ProducedFilesFolders = Directory.GetDirectories(producedFilesRootFolder);
            foreach (string folder in ProducedFilesFolders)
            {
                Directory.Delete(folder, true);
            }
        }

    }
}
