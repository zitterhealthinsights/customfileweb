﻿using FileMaker.TaskDescriptors;
using SheetDataProvider;
using System.Data;
using FileMaker.ExcelLibrary;
using System.Collections.Generic;

namespace FileMaker.Engines
{
    class SheetEngine
    {
        private SheetDescriptor SheetSettings;
        private string Folder;
        private int ClientId;

        public SheetEngine(SheetDescriptor sheetSettings, string folder, int clientId)
        {
            SheetSettings = sheetSettings;
            Folder = folder;
            ClientId = clientId;
        }

        public void Make() {

            ISheetDataReader sheetDataReader = new SheetDataReaderDatabase();
            DataTable dataTable = sheetDataReader.GetSheetData(ClientId, SheetSettings.SheetId);

            ApplyDataCustomizations(ref dataTable, SheetSettings.DataCustomizations);

            string FileLocation = Folder + SheetSettings.SheetName + Constants.EXCEL_FILE_EXTENSION;
            IExcelLibrary excelLibrary = new ExcelLibraryOfficeOpenXml();
            excelLibrary.CreateSingleSheetFile(FileLocation, dataTable, SheetSettings.SheetName, SheetSettings.SheetCustomizations);
        }

        private void ApplyDataCustomizations(ref DataTable sheetData, List<DataCustomization> dataCustomizations)
        {
            foreach (DataCustomization dc in dataCustomizations)
            {
                switch (dc)
                {
                    case DataCustomization.SORT_BY_MCOID_ASC:
                        if (sheetData.Columns.Contains("MCOID")) {
                            DataView dv = sheetData.DefaultView;
                            dv.Sort = "MCOID ASC";
                            sheetData = dv.ToTable();
                        }
                        break;

                    default:
                        break;
                }
            }
        }

    }
}
