﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using FileMaker.TaskDescriptors;
using OfficeOpenXml;
using OfficeOpenXml.Table.PivotTable;

namespace FileMaker.ExcelLibrary
{
    class ExcelLibraryOfficeOpenXml : IExcelLibrary
    {
        #region Public methods

        /// <summary>
        /// Create file with one sheet. Dump data from DataTable, then format sheet.
        /// </summary>
        /// <param name="fileLocation">Where to create the file</param>
        /// <param name="sheetData">DataTable containing sheet data</param>
        /// <param name="sheetName">Name of the sheet</param>
        /// <param name="sheetCustomizations">List of formattings to be executed</param>
        public void CreateSingleSheetFile(string fileLocation, DataTable sheetData, string sheetName, List<SheetCustomization> sheetCustomizations)
        {
            FileInfo NewFileInfo = new FileInfo(fileLocation);

            using (ExcelPackage File = new ExcelPackage(NewFileInfo))
            {
                using (ExcelWorksheet Worksheet = File.Workbook.Worksheets.Add(sheetName))
                {
                    WriteDataToSheet(Worksheet, sheetData);

                    foreach (SheetCustomization sc in sheetCustomizations)
                    {
                        switch (sc)
                        {
                            case SheetCustomization.STANDARD_FORMATTING:
                                StandardFormatting(Worksheet);
                                break;

                            case SheetCustomization.DELETE_SHEET_CONTENT:
                                Worksheet.Cells.Clear();
                                break;

                            case SheetCustomization.DATA_DICTIONARY_FORMATTING:
                                DataDictionaryFormatting(Worksheet);
                                break;

                            case SheetCustomization.DEFINITION_FORMATTING:
                                DefinitionFormatting(Worksheet);
                                break;

                            default:
                                break;
                        }
                    }

                    File.Save();
                }
                //File.Save();
            }
        }

        /// <summary>
        /// Merge several Excel files into one.
        /// </summary>
        /// <param name="resultFileLocation">Location where output file should be created</param>
        /// <param name="sourceFilesLocations">Locations of files to be merged</param>
        public void CreateMergedFile(string resultFileLocation, string[] sourceFilesLocations)
        {
            FileInfo NewFileInfo = new FileInfo(resultFileLocation);
            using (ExcelPackage ResultFile = new ExcelPackage(NewFileInfo))
            {

                foreach (string SourceFileLocation in sourceFilesLocations)
                {
                    FileInfo ExistingFileInfo = new FileInfo(SourceFileLocation);
                    using (ExcelPackage SourceFile = new ExcelPackage(ExistingFileInfo))
                    {
                        foreach (ExcelWorksheet Sheet in SourceFile.Workbook.Worksheets)
                        {
                            ResultFile.Workbook.Worksheets.Add(Sheet.Name, Sheet);
                        }
                    }
                }

                ResultFile.Save();
            }
        }

        /// <summary>
        /// Take Excel file with 1 sheet and export it to Pipe-Delimited file.
        /// Before export, clean cells content from the following chars: "|", "\r", "\n", "\t"
        /// </summary>
        /// <param name="excelFileLocation">Location of the source Excel file</param>
        /// <param name="resultFileLocation">Location where the output Pipe-Delimited file should be created</param>
        public void SingleSheetFileToPipeDelimitedFile(string excelFileLocation, string resultFileLocation) {

            FileInfo ExistingFileInfo = new FileInfo(excelFileLocation);
            using (ExcelPackage SourceFile = new ExcelPackage(ExistingFileInfo))
            {
                ExcelWorksheet Sheet = SourceFile.Workbook.Worksheets[0];

                StringBuilder sb = new StringBuilder();
                for (int rowNo = 1; rowNo <= Sheet.Dimension.End.Row; rowNo++)
                {
                    for (int colNo = 1; colNo <= Sheet.Dimension.End.Column; colNo++)
                    {

                        object CellValue = Sheet.Cells[rowNo, colNo].Value;
                        string CellText = (CellValue == null) ? "" : CellValue.ToString();
                        CellText = CellText.Replace("|", ";");
                        CellText = CellText.Replace("\r", " ");
                        CellText = CellText.Replace("\n", " ");
                        CellText = CellText.Replace("\t", " ");

                        sb.Append(CellText);
                        if (colNo < Sheet.Dimension.End.Column)
                        {
                            sb.Append("|");
                        }

                    }

                    if (rowNo < Sheet.Dimension.End.Row) {
                        sb.Append(Environment.NewLine);
                    }
                }

                File.WriteAllText(resultFileLocation, sb.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="excelFileLocation"></param>
        public void CreatePivotsSandoz(string excelFileLocation) {

            FileInfo ExistingFileInfo = new FileInfo(excelFileLocation);
            using (ExcelPackage ExcelFile = new ExcelPackage(ExistingFileInfo))
            {
                foreach (ExcelWorksheet DestinationSheet in ExcelFile.Workbook.Worksheets)
                {
                    if (DestinationSheet.Name.EndsWith(" Recent Changes"))
                    {
                        string SourceSheetName = DestinationSheet.Name.Replace(" Recent Changes", "");
                        if (SheetExists(ExcelFile, SourceSheetName))
                        {
                            ExcelWorksheet SourceSheet = ExcelFile.Workbook.Worksheets[SourceSheetName];
                            ExcelRange SourceSheetRange = GetAllCells(SourceSheet);

                            ExcelPivotTable pivotTable = DestinationSheet.PivotTables.Add(DestinationSheet.Cells[4,1], SourceSheetRange, "PT " + SourceSheetName);

                            pivotTable.GridDropZones = false;

                            AddFilterField(pivotTable, "Drug_Name");
                            AddFilterField(pivotTable, "Change_to_Entry");
                            AddFilterField(pivotTable, "Entry_Date");

                            AddRowField(pivotTable, "PayerName");
                            AddRowField(pivotTable, "Plan_Name");
                            AddRowField(pivotTable, "Reason_For_Change");
                            AddRowField(pivotTable, "Reason_for_Change_Details");
                        }
                    }
                }

                ExcelFile.Save();
            }
        }

        #endregion


        #region Data and Formatting methods
        private void WriteDataToSheet(ExcelWorksheet worksheet, DataTable sheetData) {
            worksheet.Cells[1, 1].LoadFromDataTable(sheetData, true);
        }

        private void StandardFormatting(ExcelWorksheet sht) {

            // TODO: LIBRARY PROBLEM: remove green triangle warnings "number stored as text"
            ApplyExcelDefaultFormatting(sht);

            SetAllRowsHeight(sht, 15);
            SetAllColumnsWidth(sht, 16);
            using (ExcelRange HeaderRow = GetRow(sht, 1))
            {
                SetRangeBackColor(HeaderRow, 191, 191, 191);
                HeaderRow.Style.Font.Bold = true;
                SetRangeBorders(HeaderRow, OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                HeaderRow.AutoFilter = true;
            }
        }

        private void DataDictionaryFormatting(ExcelWorksheet sht) {

            const int EmptyRowsOnTop = 2;


            using (ExcelRange FirstColumn = GetColumn(sht, 1))
            {
                SetColumnWidth(FirstColumn, 47.71);
                FirstColumn.Style.Font.Size = 10;
            }

            using (ExcelRange SecondColumn = GetColumn(sht, 2))
            {
                SetColumnWidth(SecondColumn, 66.71);
                SecondColumn.Style.Font.Size = 9;
            }

            using (ExcelRange ThirdColumn = GetColumn(sht, 3))
            {
                SetColumnWidth(ThirdColumn, 69.14);
                ThirdColumn.Style.Font.Size = 8;
            }


            using (ExcelRange FirstRow = GetRow(sht, 1))
            {
                FirstRow.Style.Font.Bold = true;
                FirstRow.Style.Font.Size = 12;
            }

            // NOTE: Database can send some empty rows at the end. So instead of using sht.Dimension.End.Row, we have to use LastNonEmptyRow
            int LastNonEmptyRow = 0;
            for (int RowNo = sht.Dimension.End.Row; RowNo >= 1; RowNo--)
            {
                string CellText1 = (sht.Cells[RowNo, 1].Value == null) ? "" : sht.Cells[RowNo, 1].Value.ToString();
                string CellText2 = (sht.Cells[RowNo, 2].Value == null) ? "" : sht.Cells[RowNo, 2].Value.ToString();
                string CellText3 = (sht.Cells[RowNo, 3].Value == null) ? "" : sht.Cells[RowNo, 3].Value.ToString();
                if ((CellText1.Length > 0) || (CellText2.Length > 0) || (CellText3.Length > 0))
                {
                    LastNonEmptyRow = RowNo;
                    break;
                }
            }

            using (ExcelRange UsedRange = sht.Cells[1, 1, LastNonEmptyRow, sht.Dimension.End.Column])
            {
                SetRangeBorders(UsedRange, OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            }

            // Insert empty rows on top
            sht.InsertRow(1, EmptyRowsOnTop);
            LastNonEmptyRow = LastNonEmptyRow + EmptyRowsOnTop;

            using (ExcelRange UsedRange = GetAllCells(sht))
            {
                UsedRange.Style.Font.Name = "Arial";
            }


            int IndicationNameRow = 0;
            int DataSectionStartRow = 0;
            int DrugNameRow = 0;
            DataDictionaryFindSpecialRows(sht, EmptyRowsOnTop, ref IndicationNameRow, ref DataSectionStartRow, ref DrugNameRow);

            // IndicationNameRow and DrugNameRow - when not found - not critical, we just move on
            // DataSectionStartRow               - when not found - critical, we stop
            if (DataSectionStartRow == 0)
            {
                sht.Cells[1, 1].Value = "Data Section Start Row not found in column A. Formatting sheet aborted.";
                return;
            }

            // Wrap text in columns 2 and 3, starting from DataSectionStartRow
            using (ExcelRange dataRange = sht.Cells[DataSectionStartRow, 2, LastNonEmptyRow, 3])
            {
                dataRange.Style.WrapText = true;
            }

            // Bold Drug Name cell
            if (DrugNameRow != 0)
            {
                sht.Cells[DrugNameRow, 2].Style.Font.Bold = true;
            }

            // Copy Indication Name into cell A1, make cell bold
            if (IndicationNameRow != 0)
            {
                sht.Cells[1, 1].Value = sht.Cells[IndicationNameRow, 3].Value;
                sht.Cells[1, 1].Style.Font.Bold = true;
                sht.Cells[1, 1].Style.Font.Size = 12;
            }

            DataDictionaryMergeAndPutDashedBorders(sht, DataSectionStartRow, LastNonEmptyRow);
        }

        private void DefinitionFormatting(ExcelWorksheet sht)
        {
            using (ExcelRange FirstColumn = GetColumn(sht, 1))
            {
                SetColumnWidth(FirstColumn, 42);
                FirstColumn.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                FirstColumn.Style.Font.Bold = true;
            }

            using (ExcelRange SecondColumn = GetColumn(sht, 2))
            {
                SetColumnWidth(SecondColumn, 170);
            }
            sht.Cells[1, 2].Style.Font.Bold = true;

        }

        #endregion


        #region Low level helper methods

        private void ApplyExcelDefaultFormatting(ExcelWorksheet sht) {

            ExcelRange sheetCells = GetAllCells(sht);

            foreach (ExcelRangeBase Cell in sheetCells)
            {
                object CellValue = Cell.Value;
                string CellText = (CellValue == null) ? "" : CellValue.ToString();
                if (CellText.Length > 0)
                {
                    int IntValue;
                    decimal DecimalValue;
                    DateTime DateValue;
                    if (int.TryParse(CellText, out IntValue))
                    {
                        Cell.Value = IntValue;
                    }
                    else if (decimal.TryParse(CellText, out DecimalValue))
                    {
                        Cell.Value = DecimalValue;
                    }
                    else if (DateTime.TryParse(CellText, out DateValue))
                    {
                        Cell.Style.Numberformat.Format = "m/d/yyyy";
                        Cell.Value = DateValue;
                    }
                    else
                    {
                        // Remove Apostrophes from string start
                        while (CellText.StartsWith("'")) {
                            CellText = CellText.Substring(1, CellText.Length - 1);
                        }

                        Cell.Style.Numberformat.Format = "@";
                        Cell.Value = CellText;
                    }
                }
            }
        }
               
        private ExcelRange GetAllCells(ExcelWorksheet sht)
        {
            return sht.Cells[1, 1, sht.Dimension.End.Row, sht.Dimension.End.Column];
        }

        private void SetAllRowsHeight(ExcelWorksheet sht, double rowHeight)
        {
            // No support for UsedRange
            for (int RowNo = sht.Dimension.Start.Row; RowNo <= sht.Dimension.End.Row; RowNo++)
            {
                sht.Row(RowNo).Height = rowHeight;
            }
        }

        private void SetAllColumnsWidth(ExcelWorksheet sht, double columnWidth)
        {
            // No support for UsedRange
            for (int ColNo = sht.Dimension.Start.Column; ColNo <= sht.Dimension.End.Column; ColNo++)
            {
                sht.Column(ColNo).Width = CorrectColumnWidth(columnWidth); // Library has a problem, width has to be increased
            }
        }

        private void SetColumnWidth(ExcelWorksheet sht, int colNumber, double columnWidth)
        {
            sht.Column(colNumber).Width = CorrectColumnWidth(columnWidth); // Library has a problem, width has to be increased
        }

        private void SetColumnWidth(ExcelRange columnRange, double columnWidth)
        {
            columnRange.Worksheet.Column(columnRange.Start.Column).Width = CorrectColumnWidth(columnWidth); // Library has a problem, width has to be increased
        }

        private ExcelRange GetRow(ExcelWorksheet sht, int rowNumber)
        {
            return sht.Cells[rowNumber, 1, rowNumber, sht.Dimension.End.Column];
        }

        private ExcelRange GetColumn(ExcelWorksheet sht, int columnNumber)
        {
            return sht.Cells[1, columnNumber, sht.Dimension.End.Row, columnNumber];
        }

        private void SetRangeBackColor(ExcelRange range, int colorRed, int colorGreen, int colorBlue)
        {
            range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            range.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(colorRed, colorGreen, colorBlue));
        }

        private void SetRangeBorders(ExcelRange range, OfficeOpenXml.Style.ExcelBorderStyle borderStyle)
        {
            // No support for XlBordersIndex.xlInsideHorizontal and XlBordersIndex.xlInsideVertical
            // We can't select the entire Range and apply borders at once, we must loop to access each cell
            for (int RowNo = range.Start.Row; RowNo <= range.End.Row; RowNo++)
            {
                for (int ColNo = range.Start.Column; ColNo <= range.End.Column; ColNo++)
                {
                    range.Worksheet.Cells[RowNo, ColNo].Style.Border.BorderAround(borderStyle);
                }
            }
        }

        private double CorrectColumnWidth(double columnWidth)
        {
            // Library Problem: Column width set to 16 - after the file is open, it's 15.29
            return columnWidth + 0.71;
        }


        private void DataDictionaryFindSpecialRows(ExcelWorksheet sht, int emptyRowsOnTopCount,
            ref int IndicationNameRow, ref int DataSectionStartRow, ref int DrugNameRow) {

            for (int RowNo = emptyRowsOnTopCount + 1; RowNo <= sht.Dimension.End.Row; RowNo++)
            {
                string CellText = (sht.Cells[RowNo, 1].Value == null) ? "" : sht.Cells[RowNo, 1].Value.ToString();

                // Look for string "Indication" in column 1
                if (CellText.Trim().Equals("Indication", StringComparison.InvariantCultureIgnoreCase) == true)
                {
                    IndicationNameRow = RowNo;
                }

                // Look for string "Drug Name" in column 1
                if (CellText.Trim().Equals("Drug Name", StringComparison.InvariantCultureIgnoreCase) == true)
                {
                    DrugNameRow = RowNo;
                }


                // Is DataSectionStartRow determined or not
                if (DataSectionStartRow == 0)
                {
                    // If empty cell found in Column 1 - DataSectionStartRow is the prevoius Row
                    if (CellText.Trim().Length == 0)
                    {
                        DataSectionStartRow = RowNo - 1;
                    }
                }

                // If all characteristic rows are found - exit for loop
                if ((IndicationNameRow != 0) && (DataSectionStartRow != 0) && (DrugNameRow != 0))
                {
                    break; // for
                }
            }
        }

        /// <summary>
        /// Merge cells in columns 1 and 2. Put dashed borders in column 3.
        /// </summary>
        private void DataDictionaryMergeAndPutDashedBorders(ExcelWorksheet sht, int dataSectionStartRow, int lastNonEmptyRow)
        {
            

            int MergeStartRow = 0;
            int MergeEndRow = 0;

            for (int RowNo = dataSectionStartRow; RowNo <= lastNonEmptyRow; RowNo++)
            {
                // NOTE: Merging section starts with a non-empty row
                //       then continues with picking the empty rows below it 

                string CellText = (sht.Cells[RowNo, 1].Value == null) ? "" : sht.Cells[RowNo, 1].Value.ToString();
                if (CellText.Trim().Length > 0)
                {
                    // Non-empty cell in Column 1
                    if (MergeEndRow > MergeStartRow)
                    {
                        // Merge cells in column 1
                        using (ExcelRange range = sht.Cells[MergeStartRow, 1, MergeEndRow, 1])
                        {
                            range.Merge = true;
                            range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            range.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                        }

                        // Merge cells in column 2
                        using (ExcelRange range = sht.Cells[MergeStartRow, 2, MergeEndRow, 2])
                        {
                            range.Merge = true;
                            range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            range.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                        }

                        // Put dashed hairline borders in column 3
                        for (int Row = MergeStartRow; Row <= MergeEndRow - 1; Row++)
                        {
                            // If we change only 1 cell - old Medium border will stay
                            sht.Cells[Row, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Hair;
                            // That's because we have to change Top border of the cell below too
                            sht.Cells[Row + 1, 3].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Hair;

                            // NOTE: for unkonwn reason "Hair" gives us the same like in Excel "Dash + Hairline"
                        }
                    }
                    MergeStartRow = RowNo;
                }
                else
                {
                    // Empty cell in Column 1
                    MergeEndRow = RowNo;
                }
            }
        }

        private bool SheetExists(ExcelPackage excelFile, string sheetName)
        {
            bool Result = false;

            foreach (ExcelWorksheet Sheet in excelFile.Workbook.Worksheets)
            {
                if (Sheet.Name == sheetName)
                {
                    Result = true;
                    break;
                }
            }

            return Result;
        }

        private void AddFilterField(ExcelPivotTable pivotTable, string fieldName)
        {
            // TODO: LIBRARY PROBLEM: no support for selecting items: all items, single item 1st one, single item specific, multiple items specific
            pivotTable.PageFields.Add(pivotTable.Fields[fieldName]);
            //pivotTable.Fields[fieldName].MultipleItemSelectionAllowed = true;
            //pivotTable.Fields[fieldName].Items[0].
        }

        private void AddRowField(ExcelPivotTable pivotTable, string fieldName)
        {
            pivotTable.RowFields.Add(pivotTable.Fields[fieldName]);
            pivotTable.Fields[fieldName].SubTotalFunctions = eSubTotalFunctions.None;
            pivotTable.Fields[fieldName].Compact = false;
            pivotTable.Fields[fieldName].Outline = false;
        }

        #endregion

    }
}
