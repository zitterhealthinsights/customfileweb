﻿using FileMaker.TaskDescriptors;
using System.Collections.Generic;
using System.Data;

namespace FileMaker.ExcelLibrary
{
    interface IExcelLibrary
    {
        /// <summary>
        /// Create Excel file with one sheet. Dump data from DataTable, then format the sheet.
        /// </summary>
        /// <param name="fileLocation">Where to create the file</param>
        /// <param name="sheetData">DataTable containing sheet data</param>
        /// <param name="sheetName">Name of the sheet</param>
        /// <param name="sheetCustomizations">List of formattings to be executed</param>
        void CreateSingleSheetFile(string fileLocation, DataTable sheetData, string sheetName, List<SheetCustomization> sheetCustomizations);

        /// <summary>
        /// Merge several Excel files into one.
        /// </summary>
        /// <param name="resultFileLocation">Location where the output file should be created</param>
        /// <param name="sourceFilesLocations">Locations of the files to be merged</param>
        void CreateMergedFile(string resultFileLocation, string[] sourceFilesLocations);

        /// <summary>
        /// Take Excel file with 1 sheet and export it to Pipe-Delimited file.
        /// Before export, clean cells content from the following chars: "|", "\r", "\n", "\t"
        /// </summary>
        /// <param name="excelFileLocation">Location of the source Excel file</param>
        /// <param name="resultFileLocation">Location where the output Pipe-Delimited file should be created</param>
        void SingleSheetFileToPipeDelimitedFile(string excelFileLocation, string resultFileLocation);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="excelFileLocation"></param>
        void CreatePivotsSandoz(string excelFileLocation);
    }
}
