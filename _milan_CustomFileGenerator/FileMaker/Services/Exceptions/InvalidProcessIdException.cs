﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileMaker.Services.Exceptions
{
    public class InvalidProcessIdException : Exception
    {
        public InvalidProcessIdException()
        {
        }

        public InvalidProcessIdException(string message)
            : base(message)
        {
        }

        public InvalidProcessIdException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
