﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileMaker.Services.Exceptions
{
    public class ProcessForbiddenForClientException : Exception
    {
        public ProcessForbiddenForClientException()
        {
        }

        public ProcessForbiddenForClientException(string message)
            : base(message)
        {
        }

        public ProcessForbiddenForClientException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
