﻿using FileMaker.Engines;
using FileMaker.Services.Exceptions;
using System;
using System.IO;

namespace FileMaker.Services
{
    public class ProcessFilesMaker
    {
        private string Folder;
        private int ProcessId;
        private int ClientId;

        public ProcessFilesMaker(string folder, int processId, int clientId)
        {
            if (folder.EndsWith(Path.DirectorySeparatorChar) == false)
            {
                folder = folder + Path.DirectorySeparatorChar;
            }
            Folder = folder;

            ProcessId = processId;
            ClientId = clientId;
        }

        /// <summary>
        /// Produces zip file
        /// </summary>
        /// <returns>Location of the produced zip file</returns>
        /// <exception cref="InvalidProcessIdException">ProcessId can not be found</exception>
        /// <exception cref="ProcessForbiddenForClientException">ClientId has no premission over ProcessId</exception>
        public string CreateProcessFiles()
        {
            string OutputZipLocation = "";

            try
            {
                ProcessEngine pe = new ProcessEngine(ProcessId, Folder, ClientId);
                OutputZipLocation = pe.Make();
            }
            catch (Exception ex)
            {
                // TODO: log
                throw ex;
            }

            return OutputZipLocation;
        }
    }
}
