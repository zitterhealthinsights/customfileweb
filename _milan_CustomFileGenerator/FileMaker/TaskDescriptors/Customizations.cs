﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileMaker.TaskDescriptors
{

    enum DataCustomization
    {
        /// <summary>
        /// Sort data table by column MCOID, ascending. If column not found, skip.
        /// </summary>
        SORT_BY_MCOID_ASC,
    }

    enum SheetCustomization
    {
        /// <summary>
        /// Set rows height, Set columns width, Set header row color, Set header row bold, Set header row border, Format all numbers as general
        /// </summary>
        STANDARD_FORMATTING,

        /// <summary>
        /// Delete all data. Dummy data from database, serving as a placeholder. Usually on sheets with Pivot Tables
        /// </summary>
        DELETE_SHEET_CONTENT,

        /// <summary>
        /// Data Dictionary sheet formatting(format 3 columns with borders, merging...)
        /// </summary>
        DATA_DICTIONARY_FORMATTING,

        /// <summary>
        /// Data Dictionary sheet formatting(Format 2 columns, 1st is right aligned...)
        /// </summary>
        DEFINITION_FORMATTING
    }

    enum FileCustomization
    {
        /// <summary>
        /// Single sheet xlsx file to pipe-delimited CSV file. Delete original xlsx file.
        /// </summary>
        LILLY_XLSX_TO_CSV,

        /// <summary>
        /// Destination: sheets with name "{INDICATION} Recent Changes". Source: sheets with name "{INDICATION}". Create Pivot Tables with names "{PT INDICATION}".
        /// Apply Sandoz settings to Pivot Tables.
        /// </summary>
        CREATE_PIVOTS_SANDOZ
    }

    enum NameTimeStampRule
    {
        /// <summary>
        /// Time of execution, date format "MMddyyyy"
        /// </summary>
        NOW__MMddyyyy,

        /// <summary>
        /// Time of execution minus 1 month, date format "MMMM yyyy"
        /// </summary>
        NOW_MINUS_ONE_MONTH__MMMM_SPACE_yyyy
    }

    enum ProcessCustomization
    {

    }
}
