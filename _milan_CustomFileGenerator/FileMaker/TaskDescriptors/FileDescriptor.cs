﻿using System.Collections.Generic;

namespace FileMaker.TaskDescriptors
{
    class FileDescriptor
    {
        public string FileName { get; set; }
        public NameTimeStampRule NameTimeStampRule { get; set; }
        public int FileIndex { get; set; }
        public List<FileCustomization> FileCustomizations;
        public List<SheetDescriptor> SheetsDescriptors;

        public FileDescriptor(string fileName, NameTimeStampRule nameTimeStampRule, int fileIndex, 
            List<FileCustomization> fileCustomizations, List<SheetDescriptor> sheetsDescriptors)
        {
            FileName = fileName;
            NameTimeStampRule = nameTimeStampRule;
            FileIndex = fileIndex;
            FileCustomizations = fileCustomizations;
            SheetsDescriptors = sheetsDescriptors;
        }
    }
}
