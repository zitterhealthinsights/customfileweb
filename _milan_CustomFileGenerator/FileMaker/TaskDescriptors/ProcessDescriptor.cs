﻿using System.Collections.Generic;

namespace FileMaker.TaskDescriptors
{
    class ProcessDescriptor
    {
        public int ProcessId { get; set; }
        public int ClientId { get; set; }
        public string ProcessName { get; set; }
        public List<ProcessCustomization> ProcessCustomizations;
        public List<FileDescriptor> FilesDescriptors;

        public ProcessDescriptor(int processId, int clientID, string processName, List<ProcessCustomization> processCustomizations, List<FileDescriptor> filesDescriptors)
        {
            ProcessId = processId;
            ClientId = clientID;
            ProcessName = processName;
            ProcessCustomizations = processCustomizations;
            FilesDescriptors = filesDescriptors;
        }
    }
}
