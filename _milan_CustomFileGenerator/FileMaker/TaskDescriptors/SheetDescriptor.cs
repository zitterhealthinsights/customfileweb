﻿using System.Collections.Generic;

namespace FileMaker.TaskDescriptors
{
    class SheetDescriptor
    {
        public int SheetId { get; set; }
        public int SheetIndex { get; set; }
        public string SheetName { get; set; }
        public List<DataCustomization> DataCustomizations;
        public List<SheetCustomization> SheetCustomizations;

        public SheetDescriptor(int sheetID, int sheetIndex, string sheetName,
            List<DataCustomization> dataCustomizations, List<SheetCustomization> sheetCustomizations)
        {
            SheetId = sheetID;
            SheetIndex = sheetIndex;
            SheetName = sheetName;
            DataCustomizations = dataCustomizations;
            SheetCustomizations = sheetCustomizations;
        }
    }
}
