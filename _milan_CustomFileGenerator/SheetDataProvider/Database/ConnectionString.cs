﻿using System.Data.SqlClient;

namespace SheetDataProvider.Database
{
    static class ConnectionString
    {
        public static string Build(DbName dbName, DbPointer dbPointer, DbLogin dbLogin)
        {

            SqlConnectionStringBuilder ConnStrBuilder = new SqlConnectionStringBuilder();

            if (dbPointer.Equals(DbPointer.LOCAL))
            {
                ConnStrBuilder.UserID = "";
                ConnStrBuilder.Password = "";
                ConnStrBuilder.IntegratedSecurity = true;
            }
            else
            {
                ConnStrBuilder.UserID = dbLogin.GetName();
                ConnStrBuilder.Password = dbLogin.GetPassword();
                ConnStrBuilder.IntegratedSecurity = false;
            }

            switch (dbName)
            {
                case DbName.JETPACK:
                    switch (dbPointer)
                    {
                        case DbPointer.LOCAL:
                            ConnStrBuilder.InitialCatalog = "JetPack_PROD";
                            ConnStrBuilder.DataSource = @"localhost\sqlexpress";
                            break;
                        case DbPointer.DEV:
                            ConnStrBuilder.InitialCatalog = "JetPack_DEV";
                            ConnStrBuilder.DataSource = "10.192.8.42\\SQLSERVER2016";
                            break;
                        case DbPointer.QA:
                            ConnStrBuilder.InitialCatalog = "JetPack_QA";
                            ConnStrBuilder.DataSource = "10.192.8.42\\SQLSERVER2016";
                            break;
                        case DbPointer.UAT:
                            ConnStrBuilder.InitialCatalog = "JETPACK_UAT";
                            ConnStrBuilder.DataSource = "TZGSRVSQL02";
                            break;
                        case DbPointer.PROD:
                            ConnStrBuilder.InitialCatalog = "JETPACK";
                            ConnStrBuilder.DataSource = "TZGSRVSQL02";
                            break;
                        default:
                            break;
                    }
                    break;

                case DbName.CWP:
                    switch (dbPointer)
                    {
                        case DbPointer.LOCAL:
                            ConnStrBuilder.InitialCatalog = "CWP_3.0";
                            ConnStrBuilder.DataSource = @"localhost\sqlexpress";
                            break;
                        case DbPointer.DEV:
                            ConnStrBuilder.InitialCatalog = "CWP_VBA_DEV";
                            ConnStrBuilder.DataSource = "10.192.8.42\\SQLSERVER2016";
                            break;
                        case DbPointer.QA:
                            ConnStrBuilder.InitialCatalog = "CWP_VBA_QA";
                            ConnStrBuilder.DataSource = "10.192.8.42\\SQLSERVER2016";
                            break;
                        case DbPointer.UAT:
                            ConnStrBuilder.InitialCatalog = "CWP_VBA_UAT";
                            ConnStrBuilder.DataSource = "10.192.8.42\\SQLSERVER2016";
                            break;
                        case DbPointer.PROD:
                            ConnStrBuilder.InitialCatalog = "CWP_3.0";
                            ConnStrBuilder.DataSource = "TZGSRV02";
                            break;
                        default:
                            break;
                    }
                    break;

                case DbName.MCMM:
                    break;

                default:
                    break;
            }

            return ConnStrBuilder.ToString();
        }
    }
}
