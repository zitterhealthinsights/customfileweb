﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace SheetDataProvider.Database
{

    enum DbName
    {
        JETPACK,
        CWP,
        MCMM
    }

    enum DbPointer
    {
        LOCAL,
        DEV,
        QA,
        UAT,
        PROD
    }

    class DataAccess
    {
        private DbName DbName;
        private DbPointer DbPointer;
        private DbLogin DbLogin;

        public DataAccess(DbName dbName, DbPointer dbPointer, DbLogin dbLogin)
        {
            DbName = dbName;
            DbPointer = dbPointer;
            DbLogin = dbLogin;
        }

        public bool CanConnectToDatabase()
        {

            bool Result = false;

            try
            {
                string ConnectionString = Database.ConnectionString.Build(DbName, DbPointer, DbLogin);

                SqlConnection Connection = new SqlConnection(ConnectionString);
                Connection.Open();
                if (Connection.State == ConnectionState.Open)
                {
                    Result = true;
                }

            }
            catch (Exception)
            {
                //throw;
            }

            return Result;
        }

        public void ExecuteStoredProcedure(string storedProcedureName, SqlParameter[] parameters, int commandTimeout = 30)
        {

            string ConnectionString = Database.ConnectionString.Build(DbName, DbPointer, DbLogin);
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                if (Connection.State == ConnectionState.Closed)
                {
                    Connection.Open();
                }

                using (SqlCommand Command = new SqlCommand(storedProcedureName, Connection))
                {
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.AddRange(parameters);
                    Command.CommandTimeout = commandTimeout;
                    Command.ExecuteNonQuery();
                }
            }
        }


        public DataTable ExecuteStoredProcedureDT(string storedProcedureName, int commandTimeout = 30)
        {
            DataTable Result = new DataTable();

            string ConnectionString = Database.ConnectionString.Build(DbName, DbPointer, DbLogin);
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand Command = new SqlCommand(storedProcedureName, Connection))
                {
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandTimeout = commandTimeout;
                    using (SqlDataAdapter DataAdapter = new SqlDataAdapter(Command))
                    {
                        DataAdapter.Fill(Result);
                    }
                }
            }

            return Result;
        }

        public DataTable ExecuteStoredProcedureDT(string storedProcedureName, SqlParameter[] parameters, int commandTimeout = 30)
        {
            DataTable Result = new DataTable();

            string ConnectionString = Database.ConnectionString.Build(DbName, DbPointer, DbLogin);
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand Command = new SqlCommand(storedProcedureName, Connection))
                {
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.AddRange(parameters);
                    Command.CommandTimeout = commandTimeout;
                    using (SqlDataAdapter DataAdapter = new SqlDataAdapter(Command))
                    {
                        DataAdapter.Fill(Result);
                    }
                }
            }

            return Result;
        }

        public SqlDataReader ExecuteStoredProcedureDR(string storedProcedureName, SqlParameter[] parameters, int commandTimeout = 30)
        {
            SqlDataReader Result = null;

            string ConnectionString = Database.ConnectionString.Build(DbName, DbPointer, DbLogin);
            SqlConnection Connection = new SqlConnection(ConnectionString);
            if (Connection.State == ConnectionState.Closed)
            {
                Connection.Open();
            }

            SqlCommand Command = new SqlCommand(storedProcedureName, Connection);
            Command.CommandType = CommandType.StoredProcedure;
            Command.Parameters.AddRange(parameters);
            Command.CommandTimeout = commandTimeout;
            Result = Command.ExecuteReader();

            return Result;
        }


    }
}
