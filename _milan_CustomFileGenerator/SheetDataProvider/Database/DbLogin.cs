﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SheetDataProvider.Database
{
    class DbLogin
    {
        private string Name;
        private string Password;

        public DbLogin(string name, string password)
        {
            Name = name;
            Password = password;
        }

        public string GetName()
        {
            return Name;
        }

        public string GetPassword()
        {
            return Password;
        }
    }
}
