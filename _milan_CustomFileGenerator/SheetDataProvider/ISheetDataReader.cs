﻿using System.Data;

namespace SheetDataProvider
{
    public interface ISheetDataReader
    {
        DataTable GetSheetData(int clientID, int sheetId);
    }
}
