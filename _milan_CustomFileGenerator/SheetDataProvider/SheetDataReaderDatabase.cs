﻿using System.Data;
using System.Data.SqlClient;
using SheetDataProvider.Database;

namespace SheetDataProvider
{
    public class SheetDataReaderDatabase : ISheetDataReader
    {
        public DataTable GetSheetData(int clientID, int sheetId)
        {
            DataAccess da = new DataAccess(DbName.CWP, Constants.DB_POINTER, Constants.DB_LOGIN);
            SqlParameter[] parameters = {
                new SqlParameter("@ClientID", clientID),
                new SqlParameter("@SheetID", sheetId)
            };

            DataTable dt = da.ExecuteStoredProcedureDT("usp_FG_GetSheetData", parameters, 600);

            return dt;
        }
    }
}
